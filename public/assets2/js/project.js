$('body').delegate('.next-page-project','click',function(){
            $.ajax({
            url: '/api/next-page-project',
            method: 'POST',
            success: function (response){
                $('#records-project').html(response.html);
                $('#show_page_project').html(response.show_page);
            }
        });
});
$('body').delegate('.forward-page-project','click',function(){
        $.ajax({
            url: '/api/forward-page-project',
            method: 'POST',
            success: function (response){
                $('#records-project').html(response.html);
                $('#show_page_project').html(response.show_page);
            }
        });
});
$('body').delegate('#check_all','change',function(){
    if ($(this).is(":checked")) {
        $('.check').each(function () {
            $(this).prop('checked', true); 
        });
    } else {
        $('.check').each(function () {
            $(this).prop('checked', false); 
        });
    }
});
$('body').delegate('.check','change',function(){
    var project_id = [];
    $('.check').each(function () {
        if ($(this).is(':checked')) {
            project_id.push($(this).val());
        }
        $('#check_all').attr('value',project_id.join(','));
    })   
})
$('body').delegate('.save-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
         $.ajax({
            url: '/api/save-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Lưu thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
                
            }
        });
    }
})
$('body').delegate('.send-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/send-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Duyệt đề án thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
            }
        });
    }
})
$('body').delegate('.remove-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("success", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/remove-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                if(response.success == 'true'){
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Đề án đã được chuyển vào thùng rác");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                }else{
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Chỉ được phép xóa đề án đang nháp");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                }
            }
        });
    }
})
$('body').delegate('.submit-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("success", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/submit-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Đề án đã được gửi");
                notification.push();
                $('input[type=checkbox]').prop('checked',false);
            }
        });
    }
})
$('body').delegate('.export-member-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined) {
        var notifier = new Notifier();
        var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/export-member-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                window.location.href=response.href;
            }
        });
    }
})
$.ajaxSetup ({
    cache: false
});
$('.load-page').click(function(){
    $('.load-page').removeClass('active');
    $(this).addClass('active');
    var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
    $(".project-content").html(ajax_load).load($(this).data('href')); 
})