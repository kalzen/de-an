@extends('mobile.layouts.master')
@section('content')
<header class="header">
    <div class="bottom-header" style="background: #333333;">
        <div class="container" style="display: flex;padding-right: 0px;">
            <div class="bottom-header-left">
                <div class="logo-header">
                    <a href="/"><img src="{!!asset('assets2/img/logo1.jpg')!!}" style="width: 103px;"></a>
                </div>
            </div>
            <div class="bottom-header-right">
                <form action="{!!route('loginMember')!!}" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group">
                        <input type="text" class="form-control" name='login_id' placeholder="COMPANY ID">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name='password' placeholder="PASSWORD">
                    </div>
                    <div class="form-group">
                        <button type='submit' class="btn btn-color">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
<div class="slider" style="position:relative">
    <img src='{!!asset('assets2/img/banner.png')!!}' style="width:100%;height:130px;">
         <div class='slide-member'>
        <div class="owl-carousel slider-carousel">
            <div class="item">
                <h5 style="font-size:12px;">{{trans('base.creatorofmonth')}}</h5>
                <table class="table-slider">
                    <thead>
                    <th></th>
                    <th >{{trans('base.name')}}</th>
                    <th >ID</th>
                    <th >Phòng</th>
                    <th >Số Đ.A</th>
                    </thead>
                    <tbody>
                        @foreach($rank_month as $key=>$val)
                        <tr>
                            <td ><span @if($key == 0) class="active" @endif>{{$key + 1}}</span></td>
                            <td  @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->full_name}}</td>
                            <td  @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->login_id}}</td>
                            <td  @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->department->name}}</td>
                            <td  @if($key == 0) class="active" @endif>{{$val->count}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="item">
                <h5>{{trans('base.creatorofyear')}}</h5>
                <table class="table-slider">
                    <thead>
                    <th></th>
                    <th>{{trans('base.name')}}</th>
                    <th>ID</th>
                    <th >Phòng</th>
                    <th >Số Đ.A</th>
                    </thead>
                    <tbody>
                        @foreach($rank_year as $key=>$val)
                        <tr>
                            <td><span @if($key == 0) class="active" @endif>{{$key + 1}}</span></td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->full_name}}</td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->login_id}}</td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->department->name}}</td>
                            <td @if($key == 0) class="active" @endif>{{$val->count}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="item">
                <h5>{{trans('base.creatorofteam')}}</h5>
                <table class="table-slider last-table-slider">
                    <thead>
                    <th></th>
                    <th>TEAM</th>
                    <th>{{trans('base.noproject')}}</th>
                    </thead>
                    <tbody>
                        @foreach($rank_team as $key=>$val)
                        <tr>
                            <td><span @if($key == 0) class="active" @endif>{{$key + 1}}</span></td>
                            <td @if($key == 0) class="active" @endif>{{$val->name}}</td>
                            <td @if($key == 0) class="active" @endif>{{$val->count}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<body class="page-body">
    <main class="main-content">
        <section class="content-section owl-carousel-spotlight carousel-spotlight ig-carousel">
            <div class="container">
                <header class="header">
                    <h5><span class="orange">TYPICAL</span> IMPROVEMENT PROJECT</h5>
                </header>
                <div class="position-relative">
                    <div id="color_sel_Carousel-content" class="tab-content fl-scn relative w-100" style="padding:20px">
                        <!-- tab item -->
                        <div class="tab-pane fade show active" id="mp-01-c" role="tabpanel" aria-labelledby="mp-01-tab">
                            <div class="owl-carousel gs-carousel" data-carousel-margin="30" data-carousel-nav="true" data-carousel-navText="<span class='icon-cl-next pe-7s-angle-left'></span>, <span class='icon-cl-next pe-7s-angle-right'></span>">
                                @foreach($slides as $key=>$slide)
                                <div class="item">
                                    <div class="item-cont">
                                        <figure class="owl_item_review">
                                            <div>
                                                <div class="position-relative overflow-hidden overlay-gradient">
                                                    <img class="m-0-auto" src="{!!$slide->image!!}" alt="{{$slide->title}}" style="height:250px">
                                                    <div class='over-text'>
                                                        <h5>{{$slide->title}}</h5>
                                                    </div>
                                                    <div class="overlay overlay-gradient"></div>
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
@stop
@section('script')
@parent
@if (Session::has('error'))
<script>
    var notifier = new Notifier();
    var notification = notifier.notify("warning", "Đăng nhập không thành công");
    notification.push();
</script>
@endif
@stop