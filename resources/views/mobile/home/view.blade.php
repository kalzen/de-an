@extends('mobile.layouts.admin')
@section('content')
<body class="page-body">
    <div class="content" style="padding: 40px 0px">
        <div class="container">
            <div class="row">
                <div class="welcome-home text-center" style="width:100%">
                    <div class="img-avatar">
                        <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {{\Auth::guard('member')->user()->avatar}} @endif" style="width:150px;">
                    </div>
                    <h3 class='orange'>{{trans('base.hello')}} {{\Auth::guard('member')->user()->full_name}}</h3>
                    <p>{{trans('base.manage.member')}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6" style="padding: 0 50px;">
                    <a href="{!!route('frontend.project.index')!!}" class='div-choose'>
                        <div class="row box-choose">
                            <div class="col-md-4" style="width:30%;text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                                <img src="{!!asset('assets2/img/tie (1).png')!!}">
                            </div>
                            <div class="col-md-8" style="width:70%;padding-left: 36px;">
                                <h4>{{trans('base.personal_page')}}</h4>
                                <p>{{trans('base.personal_page_description')}}</p>
                            </div>
                        </div>
                    </a>
                    <a href="{!!route('frontend.project.index')!!}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                <div class="col-md-6 col-sm-6" style="padding: 0 50px;">
                    <a href="{!!route('frontend.project.list')!!}" class='div-choose'>
                        <div class="row box-choose">
                            <div class="col-md-4" style="width:30%;text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                                <img src="{!!asset('assets2/img/review.png')!!}">
                            </div>
                            <div class="col-md-8" style="width:70%;padding-left: 36px;">
                                <h4>{{trans('base.project_review')}}</h4>
                                <p style="margin-bottom:0px;">{{trans('base.project_review_description1')}}</p>
                                <p>{{trans('base.project_review_description2')}}</p>
                            </div>
                        </div>
                    </a>
                    <a href="{!!route('frontend.project.list')!!}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                @if(\Auth::guard('member')->user()->level == 4 || \Auth::guard('member')->user()->level == 5)
                <div class="col-md-6 col-sm-6" style="padding: 0 50px;">
                    <a href="{!!route('frontend.member.index')!!}" class='div-choose'>
                    <div class="row box-choose">
                        <div class="col-md-4" style="width:30%;text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                            <img src="{!!asset('assets2/img/team.png')!!}">
                        </div>
                        <div class="col-md-8" style="width:70%;padding-left: 36px;">
                            <h4>{{trans('base.manage_member')}}</h4>
                            <p>{{trans('base.manage_member_description')}}</p>
                        </div>
                    </div>
                    </a>
                    <a href="{!!route('frontend.member.index')!!}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                @endif
                @if(\Auth::guard('member')->user()->level == 4)
                <div class="col-md-6 col-sm-6" style="padding: 0 50px;">
                    <a href="{{route('frontend.part.index')}}" class='div-choose'>
                    <div class="row box-choose">
                        <div class="col-md-4" style="width:30%;text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                            <img src="{!!asset('assets2/img/gear.png')!!}">
                        </div>
                        <div class="col-md-8" style="width:70%;padding-left: 36px;">
                            <h4>{{trans('base.system_management')}}</h4>
                            <p>Quản lý thông số của hệ thống Application và website<p>
                        </div>
                    </div>
                    </a>
                    <a href="{{route('frontend.part.index')}}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                @endif
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script src="https://www.gstatic.com/firebasejs/8.2.5/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.2.5/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.2.5/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.5/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.5/firebase-messaging.js"></script>
<script>
    var firebaseConfig = {
        apiKey: "AIzaSyCKRZXnutmroWJiLwdTmMiymwFm9PGKq3M",
        authDomain: "notification-aa92a.firebaseapp.com",
        projectId: "notification-aa92a",
        storageBucket: "notification-aa92a.appspot.com",
        messagingSenderId: "9641400262",
        appId: "1:9641400262:web:15e2fa3c93719419f404c7",
        measurementId: "G-MY27YC6EWR"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
    messaging.requestPermission()
    .then(function(){
        return messaging.getToken();
    })
    .then(function(token){
        console.log(token);
        $.ajax({
            url:'/api/update-device-token',
            method:'POST',
            data:{device_token:token},
            success: function(response){
            }
        })
    })
    .catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
  });
</script>
    
@stop