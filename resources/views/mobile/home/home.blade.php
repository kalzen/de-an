<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        @include('mobile/layouts/__head')
    </head>
    <body>
        <!-- Page content -->
        <div id="page">
            <!-- Main content -->
            <div class="content-wrapper">
                @include('mobile/layouts/__header_system')
                    <div class="content">
                        <div class="row" style="margin:0px;">
                            <div class="col-md-2 sidebar-content sidebar" style="min-height: 500px;padding-right:0px;">
                                <div class="card-body">
                                    <div class="media">
                                        <div class="mr-3">
                                            <a href="#"><img src="{{\Auth::guard('member')->user()->avatar}}" width="38" height="38" class="rounded-circle" alt=""></a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-title font-weight-semibold">{{\Auth::guard('member')->user()->full_name}}</div>
                                            <div class="font-size-xs opacity-50">
                                                <i class="icon-pin font-size-sm"></i> &nbsp;@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif
                                            </div>
                                        </div>
                                    </div>
                                    <a class="close-sidebar" style="position: absolute;top: 9px;right: -18px;"><i class="icon-cross" style="font-size:20px"></i></a>
                                </div>
                                <div class="nav flex-column nav-pills nav-mobile" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.part.index') active @endif" href="{!!route('frontend.part.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý bộ phận</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.department.index') active @endif" href="{!!route('frontend.department.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý phòng ban</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.groups.index') active @endif" href="{!!route('frontend.groups.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý tổ nhóm</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.team.index') active @endif" href="{!!route('frontend.team.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý team</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.level.index') active @endif" href="{!!route('frontend.level.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý cấp độ đề án</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.position.index') active @endif" href="{!!route('frontend.position.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý chức vụ</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.slide.index') active @endif" href="{!!route('frontend.slide.index')!!}" style="display:block;"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý hình ảnh tiêu biểu</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.document.index') active @endif" href="{!!route('frontend.document.index')!!}" style="display:block;height: auto;"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý tài liệu</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.config.index') active @endif" href="{!!route('frontend.config.index')!!}" style="display:block;"><img  src="{!!asset('assets2/img/send.png')!!}">Quản lý cấu hình website</a>
                                </div>
                            </div>
                            <div class="col-md-10">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                @include('mobile/layouts/__footer')
                <!-- /footer -->
            </div>
            <!-- /main content -->
        </div>
        <!-- /page content -->
    </body>
    @yield('script')   
    <script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
    <script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
    <script>
    $('.next-sidebar').click(function(){
        $('.sidebar').addClass('open');
    })
    $('.close-sidebar').click(function(){
        $('.sidebar').removeClass('open');
    })
    $('body').delegate('.btn-add', 'click', function(){
        form = $('#postData');
        url = '/api/add-' + form.data('model');
        $.ajax({
        url:url,
                method:"POST",
                data:form.serialize(),
                success:function(response){
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Thêm mới thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
                }
        })
    })
    $('.edit-data').click(function(){
        url = '/api/edit-' + $(this).data('model');
        id = $(this).data('id');
        $.ajax({
            url:url,
            method:"POST",
            data:{id:id},
            success:function(response){
                $.each(response.data, (key, item) => {
                    if(key == 'image'){
                        $('.file-drop-zone').html('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                        '<div class="kv-file-content">'+
                                                                     '<img src="'+item+'" class="file-preview-image kv-preview-data" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                         '</div>'+
                                                         '<div class="file-thumbnail-footer">'+
                                                         '<div class="file-footer-caption" title="'+item+'">'+
                                                             '<div class="file-caption-info">'+item+'</div>'+
                                                             '<div class="file-size-info"></div>'+
                                                         '</div>'+
                                                         '<div class="file-actions">'+
                                                             '<div class="file-footer-buttons file-button">'+
                                                                   '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-key="1"><i class="icon-trash"></i></button>'+

                                                               '</div>'+
                                                         '</div>'+
                                                     '</div>'+
                                                     '</div>');
                    }
                    if(key == 'select'){
                        $('select').html(item);
                        $('.select-search').select2();
                    }
                    $(`[name="${key}"]`).val(item);
                    let html = `<button type="button" class="btn btn-sm btn-default">Thoát</button>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success btn-update" data-id="` + id + `">Lưu</a>`;
                    $(`form#postData .button-group`).html(html);
                });
            }
        })
    })
    $('body').delegate('.btn-update', 'click', function(){
        form = $('#postData');
        url = '/api/update-' + form.data('model');
        id = $(this).data('id');
        $.ajax({
            url:url,
            method:"POST",
            data:form.serialize() + '&id=' + id,
            success:function(response){
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Cập nhật thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
            }
        })
    })
    $('[data-action="delete"]').click(function (e) {
        var elm = this;
        bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
        if (result === true) {
            $(elm).parents('form').submit();
        }
        });
    });
    $('#dataTables-example').dataTable({searching: false, paging: false, info: false});
    </script>
</html> 