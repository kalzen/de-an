@extends('mobile.home.home')
@section('content')
<div class="content project-content">
    <a class="next-sidebar sidebar-left"><i style="padding: 0px 10px 0px 2px;font-size: 22px;margin-top: 4px;" class="icon-arrow-right6"></i> <h4>Quản lý bộ phận</h4></a>
    <div class="form-create">
        <form id='postData' data-model='part'>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-left">Bộ phận</label>
                <div class="col-sm-6">
                    <input class="form-control" name="name" type="text"> 
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">Thành viên</label>
                <div class="col-sm-6">
                    <select  class="form-control select-search" name="member_id[]" data-placeholder="Tất cả" multiple="multiple">
                        {!!$member_html!!}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 text-center col-sm-offset-4 btn-group button-group">
                    <a href="/home" class="btn btn-sm btn-default">Quay lại</a>
                    <button type="button" class="btn btn-sm btn-primary btn-add">Thêm</button>
                </div>
            </div>
        </form>
        <table class="table datatable-basic table-bordered" id="dataTables-example" style="margin:0px">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Bộ phận</th>
                    <th>Thành viên</th>
                    <th>Tác vụ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $key=>$record)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$record->name}}</td>
                    <td>@foreach($record->member as $val)<span class="name-member"> {{$val->full_name}} </span> @endforeach</td>
                    <td>
                        <a href="javascript:void(0)" title="{!! trans('base.edit') !!}" class="success edit-data" data-id='{{$record->id}}' data-model='part'><i class="icon-pencil"></i></a>
                        <form action="{!! route('frontend.part.destroy', ['id' => $record->id]) !!}" method="POST" style="display: inline-block;margin:0px;">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                            <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                <i class="icon-close2"></i>
                            </a>              
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop