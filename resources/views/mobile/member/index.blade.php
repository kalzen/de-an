@extends('mobile.layouts.admin')
@section('content')
<body class="page-body">
    <div class="page-content">
    <div class="row" style="margin:0px;width:100%">
        <div class="col-md-12">
            <div class="content">
                <div class="row" style="padding:20px 0px;">
                    <form id="search_member">
                        <div class="col-md-1"></div>
                        <div class="col-12" style="margin-bottom: 10px;">
                            <div class="row form-inline">
                                <label class="col-3 text-right">Bộ phận</label>
                                <div class="col-8 pd0">
                                    <select class="form-control search-part select-search" name="part_id" data-placeholder="Tất cả">
                                        {!!$part_html!!}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" style="margin-bottom: 10px;">
                            <div class="row form-inline">
                                <label class="col-3 text-right">Phòng</label>
                                <div class="col-8 pd0">
                                    <select class="form-control search-department select-search" name="department_id" data-placeholder="Tất cả">
                                        {!!$department_html!!}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" style="margin-bottom: 10px;">
                            <div class="row form-inline">
                                <label class="col-3 text-right">Tổ nhóm</label>
                                <div class="col-8 pd0 select-full">
                                    <select class="form-control search-groups select-search" name="groups_id" data-placeholder="Tất cả">
                                        {!!$groups_html!!}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <div class="row form-inline">
                                <label class="col-3 text-right">Team</label>
                                <div class="col-8 pd0 select-full">
                                    <select class="form-control search-team select-search" name="team_id" data-placeholder="Tất cả">
                                        {!!$team_html!!}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-content table-member-content">
                    <div class="control-table row">
                        <div class="col-6" style="bottom: -18px;">
                            <a href="javascript:void(0)" class="list-member"><i class="icon-user-check"></i></a>
                            <a href="javascript:void(0)" class="list-member-remove"><i class="icon-user-minus"></i></a>
                        </div>
                        <div class="col-6 text-center" id="show_page_member" style="display:flex">
                                <p id='page_member'>1-{!!session('_pages')!!} trong số {!!session('_count')!!}</p>
                                 @if(session('page') > 1)
                                <a style="padding-top:15px" href="javascript:void(0)" class='forward-page-member'><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                                @endif
                                @if(session('_count') > session('_pages'))
                                <a style="padding-top:15px" href="javascript:void(0)" class='next-page-member'><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                                @endif
                        </div>
                        
                    </div>
                    <table class="table table-bordered table-member" style="margin-top:5px;">
                        <thead class="thead-dark">
                            <tr style="box-shadow: none;">
                                <th rowspan="2"><input type="checkbox" id='check_all' value='all'></th>
                                <th rowspan="2">No.</th>
                                <th rowspan="2">Bộ phận</th>
                                <th rowspan="2">Phòng</th>
                                <th rowspan="2">Tổ/Nhóm</th>
                                <th rowspan="2">Team</th>
                                <th rowspan="2">ID</th>
                                <th rowspan="2">Họ và tên</th>
                                <th rowspan="2">Chức vụ</th>
                                <th rowspan="2">Level</th>
                                <th colspan="3" class="text-center">Thông tin đề án</th>
                            </tr>
                            <tr>
                                <th>Tháng hiện tại</th>
                                <th>Quý hiện tại</th>
                                <th>Tổng số đề án</th>
                                <!--<th>Chi tiết</th>-->
                            </tr>
                        </thead>
                        <tbody id='records-member'>
                            @foreach($records as $key=>$record)
                            <tr>
                                <td><input type="checkbox" value="{{$record->id}}" name="member_id" class='check'></td>
                                <td>{{$key + 1}}</td>
                                <td>@if($record->part) {{$record->part->name}} @endif</td>
                                <td>@if($record->department) {{$record->department->name}} @endif</td>
                                <td>@if($record->groups) {{$record->groups->name}} @endif</td>
                                <td>@if($record->team) {{$record->team->name}} @endif</td>
                                <td>{{$record->login_id}}</td>
                                <td><a href="javascript:void(0)" class="edit-member" data-id="{{$record->id}}">{{$record->full_name}}</a></td>
                                <td>@if($record->position) {{$record->position->name}} @endif</td>
                                <td>{{$record->level}}</td>
                                <td>{{$record->project_month}}/{{$record->total_project_month}}</td>
                                <td>{{$record->project_quarter}}/{{$record->total_project_quarter}}</td>
                                <td>{{$record->count_project}}/{{$record->total_count_project}}</td>
                               <!-- <td></td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
<div class="modal fade" id="modal_create_member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="padding-bottom:0px">
            <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Thêm</span> thành viên</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              <form method="post" id='create_member' action='{{route('frontend.member.store')}}'>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Phòng</lable>
                          <div class='col-md-7 pd0'>
                            <select class="form-control select-search" name='department_id' id="validate_department_id" data-placeholder='Tất cả'>
                                {!!$department_html!!}
                            </select>
                            <div class="invalid-feedback" id="error_department"></div>  
                          </div>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Tổ/Nhóm</lable>
                          <div class='col-md-7 pd0'>
                            <select class="form-control select-search" name='groups_id' data-placeholder="Tất cả"> 
                                {!!$groups_html!!}
                            </select>
                          </div>
                      </div>
                      <div class="form-group form-inline row">
                            <lable class="col-md-5">Chức vụ</lable>
                            <div class="col-md-7 pd0">
                                <select class="form-control select-search" name='position_id' id="validate_position_id" data-placeholder="Tất cả">
                                    {!!$position_html!!}
                                </select>
                                <div class="invalid-feedback" id="error_position"></div>
                            </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-5">ID*</lable>
                          <div class="col-md-7 pd0">
                            <input class="form-control check-ID" type="text" name='login_id' id="validate_login_id" required="">
                            <div class="invalid-feedback" id="error_login"></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-5">
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Level*</lable>
                          <select class="form-control col-md-7" name='level' id="validate_level" required="">
                              <option value='1'>1</option>
                              <option value='2'>2</option>
                              <option value='3'>3</option>
                              <option value='4'>4</option>
                              <option value='5'>5</option>
                          </select>
                          <div class="invalid-feedback" id="error_level"></div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-5">Họ tên*</lable>
                          <div class="col-md-7 pd0">
                            <input class="form-control" type="text" name='full_name' id="validate_full_name" required="">
                            <div class="invalid-feedback" id="error_full_name"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-5">Mật khẩu*</lable>
                          <div class="col-md-7 pd0">
                            <input class="form-control password" type="password" name='password' id="validate_password" required="">
                            <div class="invalid-feedback" id="error_password"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-5">Xác nhận MK*</lable>
                          <div class="col-md-7 pd0">
                            <input class="form-control confirm-password" type="password" required="">
                            <div class="invalid-feedback"></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-3">
                       <div class="image-avatar" style="background-image:url('{!!asset('assets2/img/background-avatar.jpg')!!}')">
                       <div id="yourBtn" onclick="getFile()">Upload ảnh</div>
                       <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile" type="file" value="upload" onchange="sub(this)" /></div>
                       <input type='hidden' class='image_data' name='avatar'>
                      </div>
                  </div>
              </div>
              <div class='row'>
                  <div class='col-md-12 form-group'>
                       <label>Ghi chú</label>
                       <textarea class='form-control' rows="6" name='note'></textarea>
                  </div>
              </div>
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <div class='row' style="width:100%;">
            <div class='col-md-6'>
                <button type="button" class="btn btn-primary submit-create-member" style="background: orange;color:#fff"><i class='icon-user-plus' style="color: #fff"></i>Thêm nhân viên</button>
            </div>
            <div class='col-md-6'>
                <div class='form-upload' style="display:flex;">
                     <div style="width: 70%;">Sử dụng khi Upload DS là file theo định dạng *CSV có sẵn</div>
                     <div id="upload_excel" onclick="getFile1()">UPLOAD</div>
                     <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile1" type="file" /></div>
                 </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_edit_member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="padding-bottom:0px">
            <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Sửa</span> thành viên</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              <form method="post" id='update_member'>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
              <input type="hidden" name="id" id="update_member_id" />
                <div class="row">
                  <div class="col-md-4">
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Phòng</lable>
                          <div class="col-md-7 form-inline pd0">
                            <select class="form-control edit-department select-search" name='department_id' data-placeholder='Tất cả' >
                            </select>
                          </div>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Tổ/Nhóm</lable>
                          <div class="col-md-7 pd0">
                            <select class="form-control edit-groups select-search" name='groups_id' data-placeholder="Tất cả">
                            </select>
                          </div>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Chức vụ</lable>
                          <div class="col-md-7 pd0">
                            <select class="form-control edit-position select-search" name='position_id' data-placeholder="Tất cả">
                            </select>
                          </div>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">ID</lable>
                          <input class="form-control col-md-7 edit-login_id" type="text" name='login_id'>
                      </div>
                  </div>
                  <div class="col-md-5">
                       <div class="form-group form-inline row">
                          <lable class="col-md-5">Level</lable>
                          <select class="form-control col-md-7 edit-level" name='level'>
                          </select>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Họ tên</lable>
                          <input class="form-control col-md-7 edit-full_name" type="text" name='full_name'>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Mật khẩu</lable>
                          <input class="form-control col-md-7 edit-password" type="password" name='password'>
                      </div>
                      <div class="form-group form-inline row">
                          <lable class="col-md-5">Xác nhận MK</lable>
                          <input class="form-control col-md-7 edit-confirm-password" type="password">
                      </div>
                  </div>
                  <div class="col-md-3">
                       <div class="image-avatar" style="background-image:url('{!!asset('assets2/img/background-avatar.jpg')!!}')">
                       <div id="yourBtn" onclick="getFile()">Upload ảnh</div>
                       <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile" type="file" value="upload" onchange="sub(this)" /></div>
                       <input type='hidden' class='image_data' name='avatar'>
                      </div>
                  </div>
              </div>
              <div class='row'>
                  <div class='col-md-12 form-group'>
                       <label>Ghi chú</label>
                       <textarea class='form-control edit-note' rows="6" name='note'></textarea>
                  </div>
              </div>
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <div class='row' style="width:100%;">
            <div class='col-md-2' id="forward_member">
                <!--<a type="button" class="btn btn-primary forward-member" style="background: orange;color:#fff"><i class='icon-arrow-left7' style="color: #fff"></i> Foward</a>-->
            </div>
            <div class='col-md-7 text-center'>
                <button type="button" class="btn btn-primary submit-edit-member" style="background: orange;color:#fff"><i class='icon-pencil' style="color: #fff;margin-right: 8px;"></i>Sửa thông tin</button>
            </div>
            <div class='col-md-2' id="next_member">
                <!--<a type="button" class="btn btn-primary next-member" style="background: orange;color:#fff">Next <i class='icon-arrow-right7' style="color: #fff"></i></a>-->
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="zoom">
  <a class="zoom-fab zoom-btn-large" id="zoomBtn"><i class="icon-menu3" style="font-size: 30px;color: #fff;"></i></a>
  <ul class="zoom-menu">
    <li><a class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out modal-create-member" href="javascript:void(0)"><i class="icon-user-plus" style="font-size: 30px;color: green;"></i></a></li>
    <li><a class="zoom-fab zoom-btn-sm zoom-btn-doc scale-transition scale-out modal-edit-member" href="javascript:void(0)"><i class="icon-pencil" style="font-size: 24px;color: #333;"></i></a></li>
    <li><a class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out remove-member" href="javascript:void(0)"><i class="icon-bin" style="font-size: 24px;color: red;"></i></a></li>
    <li><a class="zoom-fab zoom-btn-sm zoom-btn-report scale-transition scale-out export-member" href="javascript:void(0)"><img src="http://de-an.local/assets2/img/excel.png"></a></li>
  </ul>
</div>
@stop
@section('script')
@parent
<script>
    $('#zoomBtn').click(function() {
        $('.zoom-btn-sm').toggleClass('scale-out');
        if (!$('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
      });

      $('.zoom-btn-sm').click(function() {
        var btn = $(this);
        var card = $('.zoom-card');
        if ($('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
        if (btn.hasClass('zoom-btn-person')) {
          card.css('background-color', '#d32f2f');
        } else if (btn.hasClass('zoom-btn-doc')) {
          card.css('background-color', '#fbc02d');
        } else if (btn.hasClass('zoom-btn-tangram')) {
          card.css('background-color', '#388e3c');
        } else if (btn.hasClass('zoom-btn-report')) {
          card.css('background-color', '#1976d2');
        } else {
          card.css('background-color', '#7b1fa2');
        }
      });
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true); 
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false); 
            });
        }
    });
    $('body').delegate('.check','change',function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                member_id.push($(this).val());
            }
            $('#check_all').attr('value',member_id.join(','));
        })   
    })
    $('body').delegate('.modal-edit-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
        }
        $.ajax({
            url: '/api/edit-list-member',
            method: 'POST',
            data: {member_id: member_id},
            dataType: 'json',
            success: function (response) {
                    $('.edit-part').html(response.part_html);
                    $('.edit-department').html(response.department_html);
                    $('.edit-groups').html(response.groups_html);
                    $('.edit-team').html(response.team_html);
                    $('.edit-position').html(response.position_html);
                    $('.edit-login_id').val(response.login_id);
                    $('.edit-level').html(response.level_html);
                    $('#update_member_id').val(response.id);
                    $('.edit-full_name').val(response.full_name);
                    $('.edit-note').val(response.note)
                    if(response.avatar !== null){
                        $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                        $('.image_data').val(response.avatar);
                    }else{
                        $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                        $('.image_data').val('');
                    }
                    if(response.next_id != null){
                        $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next_id+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>')
                    }
                    $('#modal_edit_member').modal('show');
            }
        });
    });
    $('body').delegate('.remove-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
        }
        $.ajax({
            url: '/api/remove-member',
            method: 'POST',
            data: {member_id: member_id},
            success: function (response) {
                $('#records-member').html(response.html);
                $('#show_page_member').html(response.show_page);
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Xóa thành công");
                notification.push();
            }
        });
    })
    $('body').delegate('.detail-member','click',function(){
            $.ajax({
            url: '/api/edit-detail-member',
            method: 'POST',
            data: {member_id: $(this).data('member_id')},
            dataType: 'json',
            success: function (response) {
                    $('.edit-part').html(response.part_html);
                    $('.edit-department').html(response.department_html);
                    $('.edit-groups').html(response.groups_html);
                    $('.edit-team').html(response.team_html);
                    $('.edit-position').html(response.position_html);
                    $('.edit-login_id').val(response.login_id);
                    $('#update_member_id').val(response.id);
                    $('.edit-level').html(response.level_html);
                    $('.edit-full_name').val(response.full_name);
                    $('.edit-note').val(response.note)
                   if(response.avatar !== null){
                        $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                        $('.image_data').val(response.avatar);
                    }else{
                        $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                        $('.image_data').val('');
                    }
                    if(response.next !== null){
                        $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>');
                    }else{
                        $('#next_member').html('');
                    }
                    if(response.prev !== null){
                        $('#forward_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.prev+' style="background: orange;color:#fff"><i class="icon-arrow-left7" style="color: #fff"></i> Foward</a>');
                    }else{
                        $('#forward_member').html('');
                    }
                    $('#modal_edit_member').modal('show');
            }
        });
    });
    $('body').delegate('.next-page-member','click',function(){
            $.ajax({
            url: '/api/next-page-member',
            method: 'POST',
            success: function (response){
                $('#records-member').html(response.html);
                $('#show_page_member').html(response.show_page);
            }
        });
    });
    $('body').delegate('.forward-page-member','click',function(){
            $.ajax({
                url: '/api/forward-page-member',
                method: 'POST',
                success: function (response){
                    $('#records-member').html(response.html);
                    $('#show_page_member').html(response.show_page);
                }
            });
    });
    $('body').delegate('.list-member','click',function(){
        $.ajax({
                url: '/api/list-member',
                method: 'POST',
                success: function (response){
                    $('#records-member').html(response.html);
                    $('.select-search').val(null).trigger('change');
                    $('#show_page_member').html(response.show_page);
                    $('.action-member').html(`  <li><a href="#"><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
                                                <li><a href="javascript:void(0)" class="modal-create-member"><i class="icon-user-plus" style="font-size: 30px;color: green;"></i></a></li>
                                                <li><a href="javascript:void(0)" class='modal-edit-member'><i class="icon-pencil" style="font-size: 24px;color: #333;"></i></a></li>
                                                <li><a href="javascript:void(0)" class="remove-member"><i class="icon-bin" style="font-size: 24px;color: red;"></i></a></li>`);
                }
        });
    })
    $('body').delegate('.list-member-remove','click',function(){
        $.ajax({
                url: '/api/list-member-remove',
                method: 'POST',
                success: function (response){
                    $('#records-member').html(response.html);
                    $('.select-search').val(null).trigger('change');
                    $('#show_page_member').html(response.show_page);
                    $('.action-member').html(`  <li><a href="#"><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
                                                <li><a href="javascript:void(0)" class="restore-member"><i class="icon-undo2" style="font-size: 30px;color: green;"></i></a></li>
                                            `);
                }
        });
    })
    $('body').delegate('.restore-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/restore-member',
                method: 'POST',
                data: {member_id: member_id},
                success: function (response) {
                    $('#records-member').html(response.html);
                    $('#show_page_member').html(response.show_page);
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Khôi phục thành viên thành công");
                    notification.push();
                }
            });
        }
    })
    $('body').delegate('.export-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/export-member',
                method: 'POST',
                data: {member_id: member_id},
                success: function (response) {
                    window.location.href=response.href;
                }
            });
        }
    })
    $('.search-part,.search-department,.search-groups,.search-team').change(function(){
         $('#search_member').submit();
    })
    $("#search_member").submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
               type: "POST",
               url: '/api/search-member',
               data: form.serialize(),
               success: function(response){
                    if(response.success == true){
                        $('#records-member').html(response.html);
                        $('#show_page_member').html(response.show_page);
                    }
                }
        });   
    });
    $('#upfile1').change(function(){
        var form_data = new FormData(); 
        form_data.append("files", $(this)[0].files[0]);
        var form = $(this);
        $.ajax({
                type: "POST",
                url: '/api/import-member',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Thêm thành viên thành công");
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 2000);
                    }else{
                        var notifier = new Notifier();
                        var notification = notifier.notify("info", response.message);
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 3000);
                    }
                }
        });   
    })
     $('.check-ID').blur(function(){
        var id=$(this).val();
        $.ajax({
                type: "POST",
                url: '/api/checkid-member',
                data: {id:id},
                success: function(response){
                    if(response.success == 'true'){
                        var notifier = new Notifier();
                        var notification = notifier.notify("error", "ID thành viên đã tồn tại");
                        notification.push();
                        }
                    }
                });   
    });
    $('body').delegate('.edit-member','click',function(){
        var member_id = $(this).data('id'); 
        var level = {!!\Auth::guard('member')->user()->level!!}
        if(level == 4){
            $.ajax({
                url: '/api/edit-list-member',
                method: 'POST',
                data: {member_id: member_id},
                dataType: 'json',
                success: function (response) {
                        $('.edit-part').html(response.part_html);
                        $('.edit-department').html(response.department_html);
                        $('.edit-groups').html(response.groups_html);
                        $('.edit-team').html(response.team_html);
                        $('.edit-position').html(response.position_html);
                        $('.edit-login_id').val(response.login_id);
                        $('.edit-email').val(response.email);
                        $('.edit-level').html(response.level_html);
                        $('#update_member_id').val(response.id);
                        $('.edit-full_name').val(response.full_name);
                        $('.edit-note').val(response.note)
                        if(response.avatar !== null){
                            $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                            $('.image_data').val(response.avatar);
                        }else{
                            $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                            $('.image_data').val('');
                        }
                        if(response.next_id != null){
                            $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next_id+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>')
                        }
                        $('#modal_edit_member').modal('show');
                }
            });
        }
    });
</script>
@stop