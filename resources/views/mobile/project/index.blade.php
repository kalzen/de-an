@extends('mobile.layouts.admin')
@section('content')
<body class="page-body">
    <div class="row" style="margin:0px;width:100%;">
        @include('mobile/project/sidebar')
        <div class="col-md-12 project-content" style="padding:0px;">
            <div class="top-sidebar">
                <div class="top-left-sidebar" style="width:66%;float:left">
                    <ul style="padding-top:14px;padding-left:0px;">
                        <li><a class="next-sidebar"><i style="padding: 0px 10px 0px 2px;font-size: 22px;margin-top: 4px;" class="icon-arrow-right6"></i></a></li>
                        <li style="position:relative"><input class='checkbox-action-project' type="checkbox" id='check_all' value='all'></li>
                        <li><a href="javascript:void(0)" class="save-project"><img  src="{!!asset('assets2/img/save (2).png')!!}"></a></li>
                        <li><a href="javascript:void(0)" class="remove-project"><img  src="{!!asset('assets2/img/trash (2).png')!!}"></a></li>
                        <!--<li><a href="#"><img  src="{!!asset('assets2/img/tag.png')!!}"></a></li>-->
                        @if($keyword =='draft')
                        <li><a href="submit-project"><img src="{!!asset('assets2/img/submit.png')!!}"></a></li>
                        @endif
                    </ul>
                </div>
                <div class="top-right-sidebar">
                    <div style="display:flex" id="show_page_project">
                        <p style="padding-top:15px;padding-right:10px">1-{!!session('_p_pages')!!} trong số {!!session('_p_count')!!}</p>
                        @if(session('p_page') > 1)
                        <a style="padding-top:15px" href="javascript:void(0)" class='forward-page-project'><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                        @endif
                        @if(session('_p_count') > session('_p_pages'))
                        <a style="padding-top:15px" href="javascript:void(0)" class='next-page-project'><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="content" style="overflow-y: scroll;width:100%;height:825px;">
                <table class="table table-content" style="width:100%">
                    <tbody id="records-project">
                        @foreach($records as $key=>$record)
                        <tr>
                            <td colspan="1"><input style="width:20px;height:20px;" type="checkbox" value="{{$record->id}}" name="project_id" class='check'></td>
                            
                            <td>
                                @if($record->status == \App\Project::STATUS_CANCEL)
                                    <span class="badge badge-danger">Trả về</span>
                                @elseif($record->status == 0)
                                    <span class="badge badge-secondary">Nháp</span>
                                @elseif($record->status < \App\Project::STATUS_ACTIVE)
                                    <span class="badge badge-secondary">Chờ duyệt</span>
                                @else
                                    <span class="badge badge-success">Đã duyệt</span>
                                @endif
                                <!--<span class="text-center">@if($record->levels) Level {{$record->levels->name}} @else --- @endif</span>-->
                                <span style="float:right;font-size:12px">{{date('d',strtotime($record->created_at))}} thg {{date('m',strtotime($record->created_at))}}</span>
                                <a href="{!!route('frontend.project.edit',$record->id)!!}" style="display:block;"><span>{{$record->name}}</span></a>
                            </td>
                                               
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script>
    $('.next-sidebar').click(function(){
        $('.sidebar').addClass('open');
    })
    $('.close-sidebar').click(function(){
        $('.sidebar').removeClass('open');
    })
</script>
@stop