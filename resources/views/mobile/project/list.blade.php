@extends('mobile.layouts.admin')
@section('content')
<body class="page-body">
    <div class="page-content">
    <div class="row" style="margin:0px;width:100%;">
        <div class="col-md-12">
            <div class="container">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-4" style="padding:0px;">
                        <fieldset>
                            <legend><span class="orange">TOP TEAMS</span> of Month</legend>
                            @foreach($rank_team as $key=>$val)
                            <p>{{$key + 1}}. Team {{$val->name}} <span>{{$val->count}} đề án</span></p>
                            @endforeach
                        </fieldset>
                    </div>
                    <div class="col-md-8" style="padding:0px;">
                        <fieldset>
                            <legend><span class="orange">TOP USER</span> of Quarter</legend>
                            <table class="top-user" style="margin-left:0px;">
                                <tbody>
                                    @foreach($rank_quarter as $key=>$val)
                                    <tr>
                                        <td><p>{{$key + 1}}. {{\App\Member::find($val->id)->full_name}}</p></td>
                                        <td style="float: right"><p>{{$val->count}} đề án</p></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="fillter-project text-center">
                    <ul>
                        <li><a href="{{route("frontend.project.list")}}" class="link-list @if(!isset($_GET['keyword_project'])) orange @endif" data-keyword_project="all">{{trans('base.ALL')}}</a></li>
                        @if(\Auth::guard('member')->user()->level != \App\Member::LEVEL_1 && \Auth::guard('member')->user()->level != \App\Member::LEVEL_ADMIN)
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'pending'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'pending')orange @endif ">{{trans('base.PROJECT_PENDING')}}</a></li>
                        @endif
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'return'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'return')orange @endif">{{trans('base.PROJECT_RETURN')}}</a></li>
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'approved'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'approved')orange @endif">{{trans('base.PROJECT_APROVED')}}</a></li>
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'save_member'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'save_member')orange @endif" >{{trans('base.PROJECT_SAVED')}}</a></li>
                        <li><a href="javascript:void(0)" class="chart-list" data-href='{{route('frontend.project.chart')}}'>{{trans('base.STATISTICAL_CHART')}}</a></li>
                    </ul>
                </div>
                <div class="table-content table-member-content table-project">
                    
                    <table class="table table-bordered datatable-project-mobile table-member" style="margin-top:5px;">
                        <thead class="thead-dark">
                            <tr style="box-shadow: none;">
                                <th><input type="checkbox" id='check_all' value='all'></th>
                                <th>STT</th>
                                <th>Họ và tên</th>
                                <th>Đơn vị</th>
                                <th class='text-center'>Tên đề tài</th>
                                <th>Cấp độ</th>
                                <th>Trạng thái</th>
                                <th>Ngày</th>
                            </tr>
                        </thead>
                        <tbody id='records_list_project'>
                            @foreach($records as $key=>$record)
                            <tr>
                                <td class="middle"><input type="checkbox" value="{{$record->id}}" name="member_id" class='check'></td>
                                <td  class="middle">{{$key + 1}}</td>
                                <td  class="middle">@if($record->member->is_deleted == 1) <span class="red">{{$record->member->full_name}}</span> @else {{$record->member->full_name}} @endif</td>
                                <td  class="middle">@if($record->member->department){{$record->member->department->name}} @endif</td>
                                <td><a href="{!!route('frontend.project.view',$record->id)!!}">{{$record->name}}</a></td>
                                <td class="middle">@if($record->levels)<span class="badge badge-danger">{{$record->levels->name}}</span>@else<span class="badge badge-secondary">Không cấp độ</span> @endif</td>
                                <td class="middle">
                                @if($record->status == \App\Project::STATUS_CANCEL)
                                     <span class="badge badge-danger">Trả về</span>
                                @elseif($record->status < \App\Project::STATUS_ACTIVE )
                                     <span class="badge badge-secondary">Chờ duyệt</span>
                                @else
                                     <span class="badge badge-success">Đã duyệt</span>
                                @endif</td>
                                <td class="middle">{{$record->created_at()}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class='show-chart'>
                   
                </div>
            </div>
        </div>
    </div>
</body>
</div>
<div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn"><i class="icon-menu3" style="font-size: 24px;color: #fff;"></i></a>
    <ul class="zoom-menu"
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-doc scale-transition scale-out save-project" href="javascript:void(0)"><img  src="{!!asset('assets2/img/save (1).png')!!}" style="width:24px"></a></li>
    </ul>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
<script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
<script>
    $('#zoomBtn').click(function() {
        $('.zoom-btn-sm').toggleClass('scale-out');
        if (!$('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
      });

      $('.zoom-btn-sm').click(function() {
        var btn = $(this);
        var card = $('.zoom-card');
        if ($('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
        if (btn.hasClass('zoom-btn-person')) {
          card.css('background-color', '#d32f2f');
        } else if (btn.hasClass('zoom-btn-doc')) {
          card.css('background-color', '#fbc02d');
        } else if (btn.hasClass('zoom-btn-tangram')) {
          card.css('background-color', '#388e3c');
        } else if (btn.hasClass('zoom-btn-report')) {
          card.css('background-color', '#1976d2');
        } else {
          card.css('background-color', '#7b1fa2');
        }
      });
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true); 
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false); 
            });
        }
    });
    $('body').delegate('.check','change',function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                member_id.push($(this).val());
            }
            $('#check_all').attr('value',member_id.join(','));
        })   
    })
    $('body').delegate('.next-list-project','click',function(){
            $.ajax({
            url: '/api/next-list-project',
            method: 'POST',
            success: function (response){
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
            }
        });
    });
    $('body').delegate('.forward-list-project','click',function(){
            $.ajax({
                url: '/api/forward-list-project',
                method: 'POST',
                success: function (response){
                    $('#records_list_project').html(response.html);
                    $('#show_list_project').html(response.show_page);
                }
            });
    });
    $('.link-list').click(function(){
        $('.table-member-content').attr('style','display:block');
        $('.link-list').removeClass('orange');
        $('.chart-list').removeClass('orange');
        $(this).addClass('orange');
        $(".show-chart").attr('style','display:none');
        var keyword_project = $(this).data('keyword_project');
        $.ajax({
            url:"/api/getListProject",
            type:"POST",
            data:{keyword_project:keyword_project},
            success: function (response){
                if(keyword_project == 'all'){
                    window.location.href = '{!!route('frontend.project.list')!!}';
                }
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
            }
        })
    })
    $('select[name="month"],select[name="year"]').change(function(){
        var month = $('select[name="month"]').val();
        var year = $('select[name="year"]').val();
        var keyword = $('.link-list.orange').data('keyword');
        $.ajax({
            url:"/api/getListProject",
            type:"POST",
            data:{month:month,year:year,keyword:keyword},
            success: function (response){
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
            }
        })
    })
    $('body').delegate('.send-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/send-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Duyệt đề án thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked',false);
                }
            });
        }
    })
    $('body').delegate('.return-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/return-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Đề án trả về thành công");
                notification.push();
                $('input[type=checkbox]').prop('checked',false);
            }
        });
    }
})
    $('body').delegate('.save-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
             $.ajax({
                url: '/api/save-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Lưu thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked',false);
                }
            });
        }
    })
    $('.chart-list').click(function(){
        $('.link-list').removeClass('orange');
        $(this).addClass('orange');
        $('.table-member-content').attr('style','display:none');
        $(".show-chart").attr('style','display:block;text-align:center;');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
         $(".show-chart").html(ajax_load).load($(this).data('href')); 
    })
    $('body').delegate('.export-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/export-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    window.location.href=response.href;
                }
            });
        }
    })
</script>
@stop