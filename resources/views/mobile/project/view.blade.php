@extends('mobile.layouts.admin')
@section('content')
<body class="page-body">
    <div class="row" style="margin:0px;width:100%">
        <div class="container" style="padding:0px;">
            <div class="content">
                <div class="form-create">
                    <form method="POST" action="{{route('frontend.project.update',$record->id)}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-md-6">
                                <table class='table-approved'>
                                    <tbody>
                                        <tr>
                                            <td>Ngày nhập</td>
                                            <td>{{date('h:i:s d/m/Y',strtotime($record->created_at))}}</td>
                                            <td></td>
                                        </tr>
                                        @if(!is_null($logapproved))
                                            @foreach($logapproved as $key=>$val)
                                                <tr>
                                                    @if(is_null($val))
                                                        <td>Duyệt cấp độ {{$key}}</td>
                                                        <td>00:00:00 DD/MM/YY</td>
                                                        <td>------------</td>
                                                    @else
                                                        <td>Duyệt cấp độ {{$key}}</td>
                                                        <td>{{date('h:i:s d/m/Y',strtotime($val->created_at))}}</td>
                                                        <td>{{$val->member->full_name}}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @else
                                                <tr>
                                                    <td>Duyệt cấp độ</td>
                                                    <td>{{date('h:i:s d/m/Y',strtotime($record->created_at))}}</td>
                                                    <td>{{$record->member->full_name}}</td>
                                                </tr>
                                        @endif
                                    </tbody>
                                </table>
                                <ul style="padding-top:14px" class='ul-action-project-view'>
                                    @if($record->status == \App\Project::STATUS_CANCEL)
                                        <li><a href="javascript:void(0)" style="cursor: auto;background: #fd00003d;">TRẢ VỀ</a></li>
                                    @elseif($record->status == \App\Project::STATUS_ACTIVE)
                                        <li><a href="javascript:void(0)" style="cursor: auto;background-color: #00800042;">ĐÃ DUYỆT</a></li>
                                    @else
                                        <li><a href="javascript:void(0)" style="cursor: auto;">CHỜ DUYỆT</a></li>
                                    @endif
                                        <!--<li><a href="#"><img  src="{!!asset('assets2/img/excel.png')!!}" title="Xuất file excel"></a></li>-->
                                        <li><a href="javascript:void(0)" class="save-project" data-project_id="{{$record->id}}"><img  src="{!!asset('assets2/img/save (2).png')!!}" title="Lưu"></a></li>
                                    @if($record->status == $level && $record->member->department_id == \Auth::guard('member')->user()->department_id && ($record->status < \App\Project::STATUS_ACTIVE) || ($record->status == \App\Project::STATUS_ACTIVE && $record->level < 8 && \Auth::guard('member')->user()->level == 5))
                                        <li><a href="javascript:void(0)" class='modal-edit-member' data-toggle="modal" data-target="#modal_return_project" title='Trả về'><i class="icon-reply"></i></a></li>
                                        <li><a href="javascript:void(0)" class="send-project" data-project_id="{{$record->id}}"><i class="icon-forward"  title='Gửi'></i></a></a></li>
                                    @endif
                                </ul>
                            </div>
                            <div class="notification-member col-md-6">
                                <div class="row" id='info_member' style="border: 1px solid;margin: 0px;border-radius: 4px;">
                                    <div class="col-5" style='padding-left: 0px'>
                                        <div class="img-member">
                                            <img src="{!!\Auth::guard('member')->user()->avatar!!}">
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <h3>{{$record->member->full_name}}</h3>
                                        <p>Mã nhân viên: <span>{{$record->member->login_id}}</span></p>
                                        <p>Chức vụ: <span>@if($record->member->position){{$record->member->position->name}} @endif</span></p>
                                        <p>Bộ phận: <span>@if($record->member->part){{$record->member->part->name}} @endif</span></p>
                                        <p style="margin-bottom: 10px;">Team: <span>@if($record->member->team) {{$record->member->team->name}} @endif</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <h4 class="text-center">{{$record->name}}</h4>
                            <div class="before">
                                <h6 style="text-decoration: underline;"><span class="orange">TRƯỚC</span> CẢI TIẾN</h6>
                                <div class="images-project">
                                    @foreach($record->before_images_arr() as $key=>$val)
                                    <img src="{{$val}}" data-action="zoom" class="left45">
                                    @endforeach
                                </div>
                                <div class="content-project">
                                    {!!$record->before_content!!}
                                </div>
                                <h6 style="text-decoration: underline;"><span class="orange">SAU</span> CẢI TIẾN</h6>
                               <div class="images-project">
                                    @foreach($record->after_images_arr() as $key=>$val)
                                    <img src="{{$val}}" data-action="zoom" class="left45">
                                    @endforeach
                                </div>
                                <div class="content-project">
                                    {!!$record->after_content!!}
                                </div>
                                <h5 style="text-decoration: underline;">ĐỀ ÁN CẤP ĐỘ</h5>
                                <div class="form-group col-md-3" style="margin: 0px;padding:0px">
                                    @if(($record->status == $level && $record->member->department_id == \Auth::guard('member')->user()->department_id && ($record->status < \App\Project::STATUS_ACTIVE)) || \Auth::guard('member')->user()->level == 5)
                                    <select class="form-control select-search choose-level" name="level" data-placeholder="Không cấp độ">
                                        {!!$level_html!!}
                                    </select>
                                    @else
                                        @if($record->levels) {{$record->levels->name}} @else Không cấp độ @endif
                                    @endif
                                </div>
                                @if($record->status == \App\Project::STATUS_CANCEL)
                                <h5 style="text-decoration: underline;">LÍ DO TRẢ VỀ</h5>
                                <p>{{$record->reason}}</p>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<div class="modal fade" id="modal_return_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">LÝ DO<span class="orange"> TRẢ VỀ</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmReason'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='submit-return-project'><i class="icon-reply" style="color:#fff"></i> Trả về </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/zoom.js') !!}"></script>
<script>
    $('#check_member').change(function(){
        if($(this).is(':checked')){
            $('.choose-member').prop('disabled', false);
        }else{
             $('.choose-member').prop('disabled', 'disabled');
        }
    })
    $('#check_level').change(function(){
        if($(this).is(':checked')){
            $('.choose-level').prop('disabled', false);
        }else{
             $('.choose-level').prop('disabled', 'disabled');
        }
    })
    $('#frmReason').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url:"/api/return-project",
            method:"POST",
            data:form.serialize(),
            success: function(response){
                    if(response.success == true){
                        $('#modal_return_project').modal('hide');
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Đề án trả về thành công");
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 2000);
                    }
            }
        })
    }); 
    $('body').delegate('.send-project','click',function(){
            var project_id = $(this).data('project_id');
            $.ajax({
            url: '/api/send-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Duyệt đề án thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
            }
        });
    })
    $('body').delegate('.save-project','click',function(){
        var project_id = $(this).data('project_id');
        $.ajax({
            url: '/api/save-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Lưu thành công");
                notification.push();
            }
        });
    })
</script>
@stop