<!-- footer -->
<footer class="footer py-9">
    <div class="top-footer">
        <div class="container" style="padding-top: 18px;">
            <div class="row">
                <div class="col-6 col-lg-6" style="padding:0px 20px">
                    <div class="title-footer">
                        <h6 class="text-uppercase fw-600">INFORMATIONS CATEGORY</h6>
                    </div>
                    <div class="nav flex-column">
                        @foreach($documents as $key=>$val)
                        <a class="nav-link" href="/{{$val->file}}"><i class="fas fa-chevron-right"></i> {{$val->name}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer" >
        <div class="bottom-footer-left">
            <div class="logo-footer text-right">
                 <a href="javascript:void(0)"><img src="{{\App\Config::first()->image}}" style="width:250px"></a>
            </div>
        </div>
        <div class="bottom-footer-right">
            <marquee width="40%">Copyright @2020 OURANSOFT TECHNOLOGY JSC. All Rights Reserved</marquee>
        </div>
    </div>
    <div class="modal fade" id="modal_reset_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Thay đổi</span> mật khẩu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='reset_password'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <lable class="col-md-4">Mật khẩu cũ</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" class='form-control old-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Mật khẩu mới</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" name='password' class='form-control news-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Nhập lại</lable>
                                        <div class="col-md-8 pd0">
                                            <input type="password" class='form-control confirm-news-password'>
                                        </div>
                                    </div>
                                    <p>(*) Chú ý: Mật khẩu tối thiểu 6 ký tự bao gồm cả số</p>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-reset-password'>Thay mật khẩu ngay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_update_avatar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Cập nhật</span> ảnh đại diện</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='update_avatar'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                                <input type="file" class="upload-image" multiple="multiple" name="avatar_upload[]" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                        <div class=" file-drop-zone">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="avatar" class="image_data">
                            </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-update-avatar'>Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="chat-message">
        <div class="chat-box">
            <div class="chat-head">
                <span class="status f-online"></span>
                <h6>Admin</h6>
                <div class="more">
                    <span class="close-mesage"><i class="fa fa-times"></i></span>
                </div>
            </div>
            <div class="chat-list">
                <ul class="ps-container ps-theme-default ps-active-y">
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 290px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 215px;"></div></div>
                </ul>
                <div class="text-box">
                    <input type='hidden' name='receiver_id' value='' id="receiver_id" />
                    <input type='file' name='file' >
                    <textarea placeholder="{!!trans('base.send_message')!!}..." name='message' id='content_message'></textarea>
                </div>
            </div>
        </div>
    </div>
</footer>
<input type='hidden' value='{{isset(\Auth::guard('member')->user()->id)? \Auth::guard('member')->user()->id : ''}}' id="private_id" />
<input type='hidden' value='' id="receiver" />
<script src="{!!asset('assets2/js/popper.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootstrap.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/parallax/parallax.js')!!}"></script>
<script src="{!!asset('assets2/js/scripts.js')!!}"></script>
<script src="{!!asset('assets2/js/select2.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{!!asset('assets2/js/custom.js')!!}"></script>
<script src="{!!asset('assets2/js/main.js')!!}" id="_mainJS" data-plugins="load"></script>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
@if(!is_null(\Auth::guard('member')->user()))
<script>
    function scrollToBottomFunc() {
            $('.ps-container').animate({
                scrollTop: $('.ps-container').get(0).scrollHeight
            }, 50);
    }
    var my_id = $('#private_id').val();
        var receiver = '';
        Pusher.logToConsole = true;
        var pusher = new Pusher('5f84d2a344876b9fea04', {
            cluster: 'ap1',
            forceTLS: true
        });
        var channel = pusher.subscribe('chat-message');
        channel.bind('send-message', function (data) {
            if (my_id === data.from) {
                $('.ps-container').append(`
                    <li class="me">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver == data.from ){
                $('.ps-container').append(`
                    <li class="you">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver != data.from){
                if($('#count-message').html() === ''){
                     $('#count-message').html(1);
                }else{
                     $('#count-message').html(parseInt($('#count-message').html()) + 1);
                }
            }
            scrollToBottomFunc()
        });
    $(document).on('keyup', '#content_message', function (e) {
        e.preventDefault();
        var message = $(this).val();
        var receiver_id = $('#receiver_id').val();
        var my_id = $('#my_id').val();
        var type = $('#type_message').val();
        if (e.keyCode == 13 && message != '' && receiver_id != '') {
            $(this).val('');
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                data: {message:message,receiver_id:receiver_id,my_id:my_id,type:type},
                success: function (data) {
                    scrollToBottomFunc();
                }
            })
        }
    });
    $('.seen-chat').click(function(){
        var id = {{\Auth::guard('member')->user()->id}};
        $.ajax({
            url: '/api/get-all-message',
            method: 'POST',
            data: {id:id},
            success: function (data) {
                $('#list-message').html(data.html);
            }
        })
    });
    $('body').delegate('.message','click',function(){
            var from = $(this).data('from');
            var to = $(this).data('to');
            receiver = $(this).data('to');
            $('.tab-message').removeClass('show');
            $.ajax({
                url: '/api/get-message',
                method: 'POST',
                data: {from: from,to:to},
                success: function (response) {
                    if (response.error === false) {
                        $('#chat-message').html(response.html);
                        scrollToBottomFunc();
                    }
                }
            });
    })
    $('body').delegate('.close-mesage','click',function(){
        $('.chat-box').removeClass("show");
        return false;
    });
    $('body').delegate('.upload-file','change', function(){
            var file_data = $(this).prop('files')[0];
            var receiver_id = $('#receiver_id').val();
            var my_id = $('#my_id').val();
            var type = $('#type_message').val();
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('_token','{{ csrf_token() }}');
            form_data.append('receiver_id',receiver_id);
            form_data.append('type',type);
            form_data.append('my_id',my_id);
            $.ajax({
                url: '/api/send-file-message',
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == true){
                        $input.val(response.image);
                    }
                }
             });
    });
    
</script>
@endif