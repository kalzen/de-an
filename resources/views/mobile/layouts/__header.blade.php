<header>
    <div class="bottom-header" style="background: #333333;">
        <div class="container">
            <div class="bottom-header-left" style="width:30%">
                <div class="logo-header">
                    <a href="@if(\Auth::guard('member')->user()->level > \App\Member::LEVEL_1) {{route('home.view')}} @else  {{route('frontend.project.index')}} @endif"><img src="{!!asset('assets2/img/logo1.jpg')!!}"></a>
                </div>
            </div>
            <div class="bottom-header-right" style="padding-top:18px;float:right;width:70%;">
                <!--<input type="text" class="input-search" name="search" placeholder="Search..">-->
                <ul class="ul-notification">
                    <li>
                        <a href="javascript:void(0)" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle seen-chat"><i class="icon-bubbles4"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id='count-message'>@if($count_message > 0){!! $count_message !!}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_message" aria-labelledby="dropdownMenuButton">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold">Tin nhắn của bạn</span>
           
                            </div>
                            <div class="dropdown-content-body body-message dropdown-scrollable">
                                <ul class="media-list" id='list-message'>

                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0">
                                <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle notification-menu"><i class="icon-bell2  bell"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="count-notification">@if (count(Auth::guard('member')->user()->unreadNotifications)){{count(Auth::guard('member')->user()->unreadNotifications)}}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_notification" aria-labelledby="dropdownMenuButton">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold" style="font-size:14px;">Thông báo của bạn</span>
                            </div>
                            <div class="dropdown-content-body dropdown-scrollable">
                                <ul class="media-list list-notification">
                                    @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                    <li class="media">
                                        <a href="javascript:void(0)" class="seen-notification" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}">
                                            <div class="media-body">
                                                <div class="media-title">
                                                    <span class="font-weight-semibold color-blue">{{$val->data['full_name']}}</span>
                                                    <span class="text-muted float-right font-size-sm">{{$val->data['time']}} ({{date('d/m',strtotime($val->created_at))}})</span>
                                                </div>
                                                <span class="black font-small">{{$val->data['content']}}</span>
                                            </div>
                                        </a>
                                    </li> 
                                    @endforeach
                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0">
                                <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
                            </div>
                        </div>
                    </li>
                    @if(\Auth::guard('member')->user()->level > 1)
                    <li style="position:relative;">
                        <a @if(\Auth::guard('member')->user()->level == 5) href="{!!route('frontend.project.list',['keyword'=>'unapproved'])!!}" @else href="javascript:void(0)" @endif>
                            <i class="fas fa-user-cog"></i>
                            @if(count(\App\Project::where('status','<',4)->whereDate('created_at','=',date('Y-m-d'))->get()) > 0 && \Auth::guard('member')->user()->level == 5)
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="unapproved">{{count(\App\Project::where('status','<',4)->whereDate('created_at',date('Y-m-d'))->get())}}</span>
                            @endif
                        </a>
                    </li>
                    @endif
                    <li class='nav-item dropdown'>
                        <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-user-circle"></i></a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="content-log-member">
                                <div class="img-avatar">
                                    <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {{\Auth::guard('member')->user()->avatar}} @endif" style="width:190px;padding:10px 20px;">
                                </div>
                                <h4 class="orange text-center">{!!\Auth::guard('member')->user()->full_name!!}</h4>
                                <div class="info-member-log">
                                    <p><span class="fl50">Mã nhân viên:</span><span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                    <p><span class="fl50">Chức vụ:</span> <span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                    <p><span class="fl50">Bộ phận:</span> <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                    <p><span class="fl50">Team: </span><span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                </div>
                            </div>
                            <div class="button-member-log text-center">
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_reset_password">Thay đổi mật khẩu</a>
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_update_avatar">Cập nhật ảnh đại diện</a>
                                <a href="{!!route('logoutMember')!!}" class="btn">Đăng xuất khỏi tài khoản</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script>
    $('#seen-chat').click(function(){
        $('#count-message').html('');
    });
    $('.actve-message').click(function(){
        $('.actve-message').removeClass('actve-message');
    });
    $('body').delegate('.seen-notification', 'click', function () {
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
        url: '/api/seen-notification',
                method: 'POST',
                data:{id:id, _token : '{!! csrf_token() !!}'},
                success: function (response) {
                if (response.error == false) {
                    // window.location.href = link;
                    window.open(link);
                }
        }
        });
    });
    var pusher = new Pusher('{!!env('PUSHER_APP_KEY')!!}', {
        encrypted: true,
        cluster: '{!!env('PUSHER_APP_CLUSTER')!!}'
    });
    var channel = pusher.subscribe('NotificationEvent');
        channel.bind('send-notification', function(data) {
            var newNotificationHtml = `
            <li class="media">
                <a href="javascript:void(0)" class="seen-notification" data-id="${data.id}" data-link="${data.link}" style="width:100%">
                    <div class="media-body">
                        <div class="media-title">
                            <span class="font-weight-semibold color-blue">${data.full_name}</span>
                            <span class="text-muted float-right font-size-sm">${data.time}</span>
                        </div>
                        <span class="black font-small">${data.content}</span>
                    </div>
                </a>
            </li>
            `;
            var member_id = `${data.member_id}`;
            if (member_id == {!!\Auth::guard('member')->user()->id!!}){
                $('.list-notification').prepend(newNotificationHtml);
                $('#count-notification').html(`${data.count}`);
            }
        }
    );
    $('.new-group').click(function(){
        $('#groupchat').modal('show');
    })
    $("body").delegate( "#frmAddGroup", "submit", function(e){
     e.preventDefault();
        $.ajax({
            url: '/api/add-group',
            method: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.error === false) {
                    $('#groupchat').modal('hide');
                    $('#frmAddGroup')[0].reset();
                    $('.custom-control-input').each(function () {
                        $(this).parent('span').removeClass("checked");
                        $(this).prop('checked', false);
                    });
                    swal('Thêm mới nhóm thành công');
                }else{
                    swal('Tên nhóm đã tồn tạo mời nhập tên khác');
                }
            }
        });
    })
</script>
    
