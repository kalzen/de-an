<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{\App\Config::first()->title}}</title>
<!-- CSS -->
<link rel="stylesheet" href="{!!asset('assets2/css/jquery.transfer.css')!!}">
<link href="{!!asset('assets2/css/fonts/etline-font.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
<link href="{!! asset('assets2/css/components.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/main.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/custom.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/select2.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/zoom.css')!!}" rel="stylesheet">
<!--<link rel="stylesheet" href="{!!asset('assets2/icon_font/css/icon_font.css')!!}">-->
<!-- Favicons -->
<link rel="apple-touch-icon" href="{!!asset('assets2/img/apple-touch-icon.png')!!}">
<link rel="icon" href="{{\App\Config::first()->favicon}}">
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
<script src="{!!asset('assets2/js/utils.js')!!}"></script>
<script src="{!!asset('assets2/js/jquery-nice-select.js')!!}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TSWJDSPKXP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TSWJDSPKXP');
</script>