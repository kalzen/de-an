<header>
    <div class="bottom-header" style="background: #333333;">
        <div class="container">
            <div class="bottom-header-left bottom-header-left-admin">
                <div class="logo-header">
                    <a href="@if(\Auth::guard('member')->user()->level > \App\Member::LEVEL_1) {{route('home.view')}} @else  {{route('frontend.project.index')}} @endif"><img src="{!!asset('assets2/img/logo1.jpg')!!}" style="margin-top:7px"></a>
                </div>
            </div>
            <!--<form action="{{route('frontend.project.list')}}" method="get">-->
            <div class="bottom-header-right" style="padding-top:18px;float:left;">
                <div class="categorie-search-box">
                    <form id="search-form" action="" method="POST">
                        <div class="form-group">
                            <select class="bootstrap-select" name="poscats">
                                <option data-route="{!!route('frontend.project.list')!!}" value="1" >{{trans('base.Project')}}</option>
                                <option data-route="{!!route('frontend.member.index')!!}" value="2" >{{trans('base.Member')}}</option>
                            </select>
                        </div>
                        <input id="searchTag" autocomplete="" class="type ui-autocomplete-input" name="keyword" value="{{isset($_GET['keywords']) ? $_GET['keywords'] : ''}}" type="text" placeholder="{{trans('base.Keyword')}}">
                        <button id="btnSearch"><i class="icon-search4" style="font-size:24px;"></i></button>
                    </form>
                </div>
                    <!--<input type="text" class="input-search" name="name" placeholder="Search..">-->
                
                <ul class="ul-notification">
                    <li>
                        <a href="javascript:void(0)" title="Tin nhắn" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle seen-chat"><i class="icon-bubbles4"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id='count-message'>@if($count_message > 0){!! $count_message !!}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold">{{trans('base.Your_message')}}</span>
                                <input id="search_member_message" name="search" type="text" style="font-size:13px;width: 50%;position: absolute;top: 9px;right: 5px;height: 25px!important;">
                            </div>
                            <div class="dropdown-content-body body-message dropdown-scrollable">
                                <ul class="media-list" id='list-message'>

                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0">
                                <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" title="Thông báo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle notification-menu"><i class="icon-bell2  bell"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="count-notification">@if (count(Auth::guard('member')->user()->unreadNotifications)){{count(Auth::guard('member')->user()->unreadNotifications)}}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold" style="font-size:14px;">{{trans('base.Your_notification')}}</span>
                            </div>
                            <div class="dropdown-content-body dropdown-scrollable">
                                <ul class="media-list list-notification">
                                    @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                    <li class="media">
                                        <a href="javascript:void(0)" class="seen-notification" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}">
                                            <div class="media-body">
                                                <div class="media-title">
                                                    <span class="font-weight-semibold color-blue">{{$val->data['full_name']}}</span>
                                                    <span class="text-muted float-right font-size-sm">{{$val->data['time']}} ({{date('d/m',strtotime($val->created_at))}})</span>
                                                </div>
                                                <span class="black font-small">{{$val->data['content']}}</span>
                                            </div>
                                        </a>
                                    </li> 
                                    @endforeach
                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0">
                                <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
                            </div>
                        </div>
                    </li>
                    @if(\Auth::guard('member')->user()->level > 1)
                    <li style="position:relative;">
                        <a @if(\Auth::guard('member')->user()->level == 5) href="{!!route('frontend.project.list',['keyword'=>'unapproved'])!!}" @else href="javascript:void(0)" @endif>
                            <i class="fas fa-user-cog"></i>
                            @if(count(\App\Project::where('status','<',4)->whereDate('created_at','=',date('Y-m-d'))->get()) > 0 && \Auth::guard('member')->user()->level == 5)
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="unapproved">{{count(\App\Project::where('status','<',4)->whereDate('created_at',date('Y-m-d'))->get())}}</span>
                            @endif
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" title="Thông tin tài khoản" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-user-circle"></i></a>
                        <div class="dropdown-menu infomation-member" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="content-log-member">
                                <div class="img-avatar">
                                    <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {{\Auth::guard('member')->user()->avatar}} @endif" style="width:250px;padding:10px 20px;">
                                </div>
                                <h4 class="orange text-center">{!!\Auth::guard('member')->user()->full_name!!}</h4>
                                <div class="info-member-log">
                                    <p><span class="fl50">{{trans('base.Employee_code')}}:</span><span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                    <p><span class="fl50">{{trans('base.Position')}}:</span><span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                    <p><span class="fl50">{{trans('base.Part')}}:</span> <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                    <p><span class="fl50">Team: </span><span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                </div>
                            </div>
                            <div class="button-member-log text-center">
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_reset_password">{{trans('base.Change_the_password')}}</a>
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_update_avatar">{{trans('base.Update_avatar')}}</a>
                                <a href="{!!route('logoutMember')!!}" class="btn">{{trans('base.Sign_out_of_your_account')}}</a>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="top-header-right">
                     <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex" style="top:8px;">
                        <li class="nav-item dropdown">
                            <a class="nav-item nav-link dropdown-toggle mr-md-2 nav-header" style="color:#fff;" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {!!trans('base.language')!!}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                                <a class="dropdown-item @if(session('locale') == 'en') active @endif" href="{{route('frontend.language.change',['locale'=>'en'])}}">{{trans('base.English')}}</a>
                                <a class="dropdown-item @if(session('locale') == 'vi') active @endif" href="{{route('frontend.language.change',['locale'=>'vi'])}}">{{trans('base.Vietnamese')}}</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
           <!-- </form>-->
        </div>
    </div>
</header>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script>
    $('#seen-chat').click(function(){
        $('#count-message').html('');
    });
    $('.actve-message').click(function(){
        $('.actve-message').removeClass('actve-message');
    });
    $('body').delegate('.seen-notification', 'click', function () {
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
        url: '/api/seen-notification',
                method: 'POST',
                data:{id:id, _token : '{!! csrf_token() !!}'},
                success: function (response) {
                if (response.error == false) {
                    //window.location.href = link;
                    window.open(link);
                }
        }
        });
    });
    var pusher = new Pusher('{!!env('PUSHER_APP_KEY')!!}', {
        encrypted: true,
        cluster: '{!!env('PUSHER_APP_CLUSTER')!!}'
    });
    var channel = pusher.subscribe('NotificationEvent');
        channel.bind('send-notification', function(data) {
            var newNotificationHtml = `
            <li class="media">
                <a href="javascript:void(0)" class="seen-notification" data-id="${data.id}" data-link="${data.link}" style="width:100%">
                    <div class="media-body">
                        <div class="media-title">
                            <span class="font-weight-semibold color-blue">${data.full_name}</span>
                            <span class="text-muted float-right font-size-sm">${data.time}</span>
                        </div>
                        <span class="black font-small">${data.content}</span>
                    </div>
                </a>
            </li>
            `;
            var member_id = `${data.member_id}`;
            if (member_id == {!!\Auth::guard('member')->user()->id!!}){
                $('.list-notification').prepend(newNotificationHtml);
                $('#count-notification').html(`${data.count}`);
            }
        }
    );
    $('.new-group').click(function(){
        $('#groupchat').modal('show');
    })
    $("body").delegate( "#frmAddGroup", "submit", function(e){
     e.preventDefault();
        $.ajax({
            url: '/api/add-group',
            method: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.error === false) {
                    $('#groupchat').modal('hide');
                    $('#frmAddGroup')[0].reset();
                    $('.custom-control-input').each(function () {
                        $(this).parent('span').removeClass("checked");
                        $(this).prop('checked', false);
                    });
                    swal('Thêm mới nhóm thành công');
                }else{
                    swal('Tên nhóm đã tồn tạo mời nhập tên khác');
                }
            }
        });
    })
    $("#btnSearch").on('click', function (e) {
        e.preventDefault();
        if (!$('#searchTag').val().trim().length) {
            $('#searchTag').focus();
            return false;
        } else if ($('#searchTag').val().trim().match(/(\/|\'|\"|NULL|null|<|>|--|-->)/i)) {
            $('#searchTag').val('');
            $('#searchTag').attr('title', lang_pack.not_valid_character_search).attr('data-toggle', 'tooltip').tooltip();
            return false;
        } else if ($('#searchTag').attr('data-original-title')) {
            $('#searchTag').tooltip('dispose');
        }
        var keyword = $('#searchTag').val();
        var link = $('[name="poscats"] option:selected').data('route');
        location.href = link + '?keywords=' + keyword;
    });
    $('.bootstrap-select').niceSelect();
</script>
    
