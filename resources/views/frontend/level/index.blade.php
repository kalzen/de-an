@extends('frontend.home.home')
@section('content')
<div class="content project-content">
    <h4 style="margin-top:25px">{{trans('base.manage_project_level')}}</h4>
    <div class="form-create">
        <form id='postData' data-model='level'>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">{{trans('base.Project_level')}}</label>
                <div class="col-sm-6">
                    <input class="form-control" name="name" type="text"> 
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-5"></div>
                <div class="col-md-7 col-sm-8 col-sm-offset-4 btn-group button-group">
                    <a href="/home" class="btn btn-sm btn-default">{{trans('base.Back')}}</a>
                    <button type="button" class="btn btn-sm btn-primary btn-add">{{trans('base.Add')}}</button>
                </div>
            </div>
        </form>
        <table class="table datatable-basic table-bordered" id="dataTables-example" style="margin:0px">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{trans('base.Level')}}</th>
                    <th>{{trans('base.Action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $key=>$record)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$record->name}}</td>
                    <td>
                        <a href="javascript:void(0)" title="{!! trans('base.edit') !!}" class="success edit-data" data-id='{{$record->id}}' data-model='level'><i class="icon-pencil"></i></a>
                        <form action="{!! route('frontend.level.destroy', ['id' => $record->id]) !!}" method="POST" style="display: inline-block;margin:0px;">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                            <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                <i class="icon-close2"></i>
                            </a>              
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop