<div style="width: 100%;display:block;">
    <canvas id="income" width="600" height="400"></canvas>
    <p>Biểu đồ thống kê đề án theo tháng</p>
</div> 
<div style="width: 100%;display:block;">
    <canvas id="countries" width="600" height="400"></canvas>
    <p>Biểu đồ thống kê đề án theo trạng thái</p>
</div>
<div style="width: 100%;display:block;">
    <canvas id="status_chart" width="600" height="400"></canvas>
    <p>Biểu đồ thống kê đề án theo cấp độ</p>
</div>
<script>
    var labels1=[]; 
    var datas1 =[];
    var labels2=[]; 
    var datas2 =[];
    @foreach($label1 as $val)
        labels1.push('{!!$val!!}');
    @endforeach
    @foreach($data1 as $key=>$val1)
        datas1.push('{!!$val1!!}');
    @endforeach
    var labels2=[]; 
    var datas2=[];
    @foreach($label2 as $val)
        labels2.push('{!!$val!!}');
    @endforeach
    @foreach($data2 as $key=>$val2)
        datas2.push('{!!$val2!!}');
    @endforeach
    var barData = {
        labels : labels1,
        datasets : [
            {
                fillColor : "#48A497",
                strokeColor : "#48A4D1",
                data : datas1
            },
        ]
    }
    var income = document.getElementById("income").getContext("2d");
    new Chart(income).Bar(barData);
    var barData1 = {
        labels : labels2,
        datasets : [
            {
                fillColor : "#48A497",
                strokeColor : "#48A4D1",
                data : datas2
            },
        ]
    }
    var income1 = document.getElementById("status_chart").getContext("2d");
    new Chart(income1).Bar(barData1);
    var pieData = [
        {
            value: {{$status_pending}},
            color:"#3a4048",
            label: "Chờ duyệt"
        },
        {
            value : {{$status_return}},
            color : "#da2f69",
            label: "Trả về"
        },
        {
            value : {{$status_approved}},
            color : "#26d847",
            label: "Đã duyệt"
        },
    ];

    // pie chart options
    var pieOptions = {
        segmentShowStroke : false,
        animateScale : true
    }
    var countries= document.getElementById("countries").getContext("2d");
    new Chart(countries).Pie(pieData, pieOptions);
    
</script>

