<div class="col-md-2">
    <div class="button-create">
        <a href="{{route('frontend.project.create')}}" class="add-project"><i class="fas fa-plus"></i> New PJ</a>
    </div>
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link load-page  @if(!isset($_GET['keyword'])) active @endif" href="{!!route('frontend.project.index')!!}"><img  src="{!!asset('assets2/img/sidebar (1).png')!!}">{{trans('base.All_project')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'save') active @endif" href="{!!route('frontend.project.index',['keyword'=>'save'])!!}"><img  src="{!!asset('assets2/img/save.png')!!}">{{trans('base.Save')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_saved',1)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'send') active @endif" href="{!!route('frontend.project.index',['keyword'=>'send'])!!}"><img  src="{!!asset('assets2/img/send.png')!!}">{{trans('base.Send')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status','>',0)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'draft') active @endif" href="{!!route('frontend.project.index',['keyword'=>'draft'])!!}"><img  src="{!!asset('assets2/img/documents.png')!!}">{{trans('base.Draft')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status',0)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'remove') active @endif" href="{!!route('frontend.project.index',['keyword'=>'remove'])!!}"><img  src="{!!asset('assets2/img/trash.png')!!}">{{trans('base.Trash')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',1)->count()!!}</span></a>
    </div>
    <div class="text-sidebar">
        <h6>{{trans('base.Your_ranking')}}</h6>
        <ul class="first-ul">
            <li>{{trans('base.Personal')}} ( @if($position_rank != 0){{$position_rank}} @else Chưa có đề án @endif)</li>
            <li>{{trans('base.Group')}} ( @if($position_team_rank != 0){{$position_team_rank}} @else Chưa có đề án @endif)</li>
        </ul>
    </div>
    <div class="text-sidebar">
        <h6><span class="orange">TOP TEAMS</span> of Month</h6>
        <ul>
            @foreach($rank_team as $key=>$val)
            <li>{{$key + 1}}. Team {{$val->name}} <span>{{$val->count}} {{trans('base.project')}}</span></li>
            @endforeach
        </ul>
    </div>
    <div class="text-sidebar">
        <h6><span class="orange">TOP USER</span> of Quarter </h6>
        <ul>
            @foreach($rank_quarter as $key=>$val)
            <li>{{$key + 1}}. <span class="full-name-member">{{\App\Member::find($val->id)->full_name}}</span> <span>{{$val->count}} {{trans('base.project')}}</span></li>
            @endforeach
        </ul>
    </div>
</div>