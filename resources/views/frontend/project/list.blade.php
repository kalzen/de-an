@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
    <div class="page-content">
    <div class="row" style="margin:0px;">
        <div class="col-md-12">
            <div class="container">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-4">
                        <fieldset>
                            <legend><span class="orange">TOP TEAMS</span> of Month</legend>
                            @foreach($rank_team as $key=>$val)
                            <p>{{$key + 1}}. Team {{$val->name}} <span>{{$val->count}} {{trans('base.project')}}</span></p>
                            @endforeach
                        </fieldset>
                    </div>
                    <div class="col-md-8">
                        <fieldset>
                            <legend><span class="orange">TOP USER</span> of Quarter</legend>
                            <table class="top-user">
                                <tbody>
                                    @foreach($rank_quarter as $key=>$val)
                                    <tr>
                                        <td><p>{{$key + 1}}. {{\App\Member::find($val->id)->full_name}}</p></td>
                                        <td><p>{{\App\Member::find($val->id)->login_id}}</p></td>
                                        <td><p>Team @if(\App\Member::find($val->id)->team){{\App\Member::find($val->id)->team->name}} @endif</p></td>
                                        <td><p>{{$val->count}} {{trans('base.project')}}</p></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="fillter-project text-center">
                    <ul>
                        <li><a href="{{route("frontend.project.list")}}" class="link-list @if(!isset($_GET['keyword_project'])) orange @endif" data-keyword_project="all">{{trans('base.ALL')}}</a></li>
                        @if(\Auth::guard('member')->user()->level != \App\Member::LEVEL_1 && \Auth::guard('member')->user()->level != \App\Member::LEVEL_ADMIN)
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'pending'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'pending')orange @endif ">{{trans('base.PROJECT_PENDING')}}</a></li>
                        @endif
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'return'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'return')orange @endif">{{trans('base.PROJECT_RETURN')}}</a></li>
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'approved'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'approved')orange @endif">{{trans('base.PROJECT_APROVED')}}</a></li>
                        <li><a href="{{route("frontend.project.list",['keyword_project'=>'save_member'])}}" class="link-list @if(isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'save_member')orange @endif" >{{trans('base.PROJECT_SAVED')}}</a></li>
                        <li><a href="javascript:void(0)" class="chart-list" data-href='{{route('frontend.project.chart')}}'>{{trans('base.STATISTICAL_CHART')}}</a></li>
                    </ul>
                </div>
                <form id="search_project" method='GET' action='{{route('frontend.project.list')}}'>
                    <input type="hidden" name="keyword_project" value="@if(isset($_GET['keyword_project'])){{$_GET['keyword_project']}}@endif">
                    <div class="row" style="padding:20px 0px;">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <div class="row form-inline">
                                    <label class="col-md-4 text-right">{{trans('base.Full_name')}}</label>
                                    <div class="col-md-8 pd0 select-full">
                                        <input class="form-control search-full_name" name="full_name" value='@if(isset($_GET['full_name'])){{$_GET['full_name']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-inline">
                                    <label class="col-md-4 text-right">{{trans('base.Department')}}</label>
                                    <div class="col-md-8 pd0 select-full">
                                        <select class="form-control search-department select-search" name="department_id" data-placeholder="{{trans('base.All')}}">
                                            {!!$department_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-inline">
                                    <label class="col-md-5 text-right">{{trans('base.Level')}}</label>
                                    <div class="col-md-7 pd0 select-full">
                                        <select class="form-control search-level select-search" name="level" data-placeholder="{{trans('base.All')}}">
                                            {!!$level_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-inline">
                                    <label class="col-md-4 text-right">{{trans('base.Status')}}</label>
                                    <div class="col-md-8 pd0 select-full">
                                        <select class="form-control search-status select-search" name="status" data-placeholder="{{trans('base.All')}}">
                                            <option></option>
                                            <option value="1,2" @if(isset($_GET['status']) && $_GET['status'] == '1,2') selected @endif>{{trans('base.Pendding')}}</option>
                                            <option value="{{\App\Project::STATUS_ACTIVE}}" @if(isset($_GET['status']) && $_GET['status'] == \App\Project::STATUS_ACTIVE) selected @endif>{{trans('base.Approved')}}</option>
                                            <option value="{{\App\Project::STATUS_CANCEL}}" @if(isset($_GET['status']) && $_GET['status'] == \App\Project::STATUS_CANCEL) selected @endif>{{trans('base.Return')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
                </form>
                <div class="table-content table-member-content">
                    <table class="table table-bordered datatable-project table-member">
                        <thead class="thead-dark">
                            <tr style="box-shadow: none;">
                                <th><input type="checkbox" id='check_all' value='all'></th>
                                <th>STT</th>
                                <th>{{trans('base.Full_name')}}</th>
                                <th>{{trans('base.Department')}}</th>
                                <th class='text-center'>{{trans('base.Subject_name')}}</th>
                                <th>{{trans('base.Level')}}</th>
                                <th>{{trans('base.Status')}}</th>
                                <th>{{trans('base.Date')}}</th>
                            </tr>
                        </thead>
                        <tbody id='records_list_project'>
                            @foreach($records as $key=>$record)
                            <tr>
                                <td class="middle"><input type="checkbox" value="{{$record->id}}" name="member_id" class='check'></td>
                                <td  class="middle">{{$key + 1}}</td>
                                <td  class="middle">@if($record->member->is_deleted == 1) <span class="red">{{$record->member->full_name}}</span> @else {{$record->member->full_name}} @endif</td>
                                <td  class="middle">@if($record->member->department){{$record->member->department->name}} @endif</td>
                                <td><a href="{!!route('frontend.project.view',$record->id)!!}">{{$record->name}}</a></td>
                                <td class="middle">@if($record->levels)<span class="badge badge-danger">{{$record->levels->name}}</span>@else<span class="badge badge-secondary">Không cấp độ</span> @endif</td>
                                <td class="middle">
                                @if($record->status == \App\Project::STATUS_CANCEL)
                                     <span class="badge badge-danger">{{trans('base.Return')}}</span>
                                @elseif($record->status == \App\Project::STATUS_ACTIVE )
                                    <span class="badge badge-success">{{trans('base.Approved')}}</span>
                                @elseif($record->status == 0 )
                                    <span class="badge badge-secondary">{{trans('base.Draft')}}</span>
                                @else
                                    <span class="badge badge-secondary">{{trans('base.Pendding')}}</span>
                                @endif
                                </td>
                                <td class="middle">{{$record->created_at()}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class='show-chart'>
                   
                </div>
            </div>
        </div>
    </div>
</body>
<div class="zoom">
    <a class="zoom-fab zoom-btn-large" href="javascript:void(0)" id="zoomBtn"><i class="icon-menu3" style="font-size: 30px;color: #fff;"></i></a>
    <ul class="zoom-menu">
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out export-project" href="javascript:void(0)"><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-doc scale-transition scale-out save-project" href="javascript:void(0)"><img  src="{!!asset('assets2/img/save (1).png')!!}" style="width:24px"></a></li>
    </ul>
</div>
</div>
<div class="modal fade" id="modal_choose_report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Chọn mẫu xuất file báo cáo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="frmExport">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-md-2">Bắt đầu</div>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="start_date" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="col-md-1">đến</div>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="end_date" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-md-2">Phòng</div>
                            <div class="col-md-4 select-full">
                                <select class="form-control select-search" name="department_id" data-placeholder="{{trans('base.All')}}">
                                    {!!$department_html!!}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export1"><input type="radio" name="type" id="export1" value="1" checked style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo cá nhân không có đề tài</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export2"><input type="radio" name="type" id="export2" value="2" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo xếp loại nhóm</span></label>
                                </div>
                            </div>
                           <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export3"><input type="radio" name="type" id="export3" value="3" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo xếp loại cá nhân theo quý</span></label>
                                </div>
                            </div>
                           <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export4"><input type="radio" name="type" id="export4" value="4" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo tổng hợp đề tài</span></label>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="button" class='submit-export'>Xuất báo cáo</button>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
<script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
<script>
      $('#zoomBtn').click(function() {
            $('.zoom-btn-sm').toggleClass('scale-out');
            if (!$('.zoom-card').hasClass('scale-out')) {
              $('.zoom-card').toggleClass('scale-out');
            }
      });
      $('.zoom-btn-sm').click(function() {
        var btn = $(this);
        var card = $('.zoom-card');
        if ($('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
        if (btn.hasClass('zoom-btn-person')) {
          card.css('background-color', '#d32f2f');
        } else if (btn.hasClass('zoom-btn-doc')) {
          card.css('background-color', '#fbc02d');
        } else if (btn.hasClass('zoom-btn-tangram')) {
          card.css('background-color', '#388e3c');
        } else if (btn.hasClass('zoom-btn-report')) {
          card.css('background-color', '#1976d2');
        } else {
          card.css('background-color', '#7b1fa2');
        }
    });
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true); 
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false); 
            });
        }
    });
    $('body').delegate('.check','change',function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                member_id.push($(this).val());
            }
            $('#check_all').attr('value',member_id.join(','));
        })   
    })
    $('body').delegate('.send-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/send-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Duyệt đề án thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked',false);
                }
            });
        }
    })
    $('body').delegate('.return-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/return-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Đề án trả về thành công");
                notification.push();
                $('input[type=checkbox]').prop('checked',false);
            }
        });
    }
})
    $('body').delegate('.save-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
             $.ajax({
                url: '/api/save-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Lưu thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked',false);
                }
            });
        }
    })
    $('.chart-list').click(function(){
        $('.link-list').removeClass('orange');
        $(this).addClass('orange');
        $('.table-member-content').attr('style','display:none');
        $(".show-chart").attr('style','display:block;text-align:center;');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
         $(".show-chart").html(ajax_load).load($(this).data('href')); 
    })
    $('body').delegate('.submit-export','click',function(){
        $.ajax({
            url: '/api/export-project',
            method: 'POST',
            data: $("#frmExport").serialize(),
            success: function (response) {
                window.location.href = response.href;
            }
        });
    })
    $('body').delegate('.export-project','click',function(){
       $('#modal_choose_report').modal('show');
    })
    $('.search-full_name,.search-department,.search-level,.search-status').change(function(){
         $('#search_project').submit();
    })
</script>
@stop