<div class="top-sidebar">
    <div class="top-left-sidebar" style="width:85%;float:left">
        <ul style="padding-top:14px">
            <li style="position:relative"><input class='checkbox-action-project' type="checkbox" id='check_all' value='all'></li>
            <li><a href="javascript:void(0)" class='export-member-project'><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
            <li><a href="javascript:void(0)" class="save-project"><img  src="{!!asset('assets2/img/save (2).png')!!}"></a></li>
            <li><a href="javascript:void(0)" class="remove-project"><img  src="{!!asset('assets2/img/trash (2).png')!!}"></a></li>
            <!--<li><a href="#"><img  src="{!!asset('assets2/img/tag.png')!!}"></a></li>-->
            @if(session('keyword') == 'draft')
            <li><a href="javascript:void(0)" class='submit-project'><img  src="{!!asset('assets2/img/submit.png')!!}"></a></li>
            @endif
        </ul>
    </div>
    <div class="top-right-sidebar">
        <div style="display:flex" id="show_page_project">
            <p style="padding-top:15px;padding-right:10px">1-{!!session('_p_pages')!!} trong số {!!session('_p_count')!!}</p>
            @if(session('p_page') > 1)
            <a style="padding-top:15px" href="javascript:void(0)" class='forward-page-project'><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
            @endif
            @if(session('_p_count') > session('_p_pages'))
            <a style="padding-top:15px" href="javascript:void(0)" class='next-page-project'><img src="{!!asset('assets2/img/forward.png')!!}"></a>
            @endif
        </div>
    </div>
    </div>
    <div class="content" style="overflow-y: scroll;width:100%;height:825px;">
    <table class="table table-content" style="width:100%">
        <tbody id="records-project">
            @foreach($records as $key=>$record)
            <tr>
                <td colspan="1"><input style="width:20px;height:20px;" type="checkbox" value="{{$record->id}}" name="project_id" class='check'></td>
                <td style="display: inline-grid;"><span class="badge badge-secondary">Chờ duyệt</span><span class="text-center">Level A</span></td>
                <td><a href="{!!route('frontend.project.edit',$record->id)!!}"><span>{{$record->name}}</span></a></td>
                <td><span>{{date('d',strtotime($record->created_at))}} tháng {{date('m',strtotime($record->created_at))}}</span></td>                               
            </tr>
            @endforeach
        </tbody>
    </table>
</div>