@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
    <div class="row" style="margin:0px;">
        @include('frontend.project.sidebar')
        <div class="col-md-10">
            <div class="content project-content">
                @if($record->status != \App\Project::STATUS_ACTIVE)
                <h4 style="margin-top:25px">{{trans('base.EDITING_IMPROVED_PROJECT_INFORMATION')}}</h4>
                @else
                <h4 style="margin-top:25px">THÔNG TIN ĐỀ ÁN CẢI TIẾN</h4>
                @endif
                <div class="form-create">
                    <form method="POST" action="{{route('frontend.project.update',$record->id)}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom: 30px;">
                            <div class="notification-member col-md-6">
                                <div class="row" id='info_member' style="border: 1px solid;margin-left: 0px;border-radius: 4px;">
                                    <div class="col-md-3" style='padding-left: 0px'>
                                        <div class="img-member" style="line-height:162px;">
                                            <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {!!\Auth::guard('member')->user()->avatar!!} @endif" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <h3>{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                        <p>{{trans('base.Employee_code')}}: <span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                        <p>{{trans('base.Position')}}: <span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                        <p>{{trans('base.Part')}}: <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                        <p>Team: <span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-1 col-form-label">{{trans('base.SUBJECT_NAME')}}</label>
                            <div class="col-sm-11">
                                <input class="form-control" name="name" type="text" value='{{$record->name}}' @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.BEFORE')}}</span> {{trans('base.IMPROVE')}}: </label>
                            <div class="col-md-6">
                                <textarea class="form-control @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif" id="before_content" name="before_content">{!!$record->before_content!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" class="upload-images @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disable @endif" multiple="multiple" name="before_upload[]" data-fouc="" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                        <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="before_images" value="{{$record->before_images}}" class="image_data">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.AFTER')}}</span> {{trans('base.IMPROVE')}}: </label>
                            <div class="col-md-6">
                                <textarea class="form-control @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif" id="after_content" name="after_content">{!!$record->after_content!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" class="upload-images @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disable @endif" multiple="multiple" name="after_upload[]" data-fouc="" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                        <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="after_images" value="{{$record->after_images}}" class="image_data">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-3" style="margin-left: 16px;">
                                <input type="checkbox" class="form-check-input" id="check_level" style="width:22px;height:22px;" @if($record->level != null) checked @endif @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif>
                                <label class="form-check-label" for="check_level" style="margin-left:10px;">{{trans('base.CONSIDER_HIGH_LEVEL_PROJECTS')}}</label>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control select-search choose-level" name="level" data-placeholder="{{trans('base.Choose_level')}}" @if($record->level == null) disabled @endif>
                                   {!!$level_html!!}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_personal"><input type="radio" name="type" id="check_personal" value="1" @if($record->type == 1) checked @endif style="width:22px;height:22px!important;" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif><span style="margin-bottom: 13px;position: absolute;left: 50px;">{{trans('base.PERSONAL_PROJECT')}}</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_team"><input type="radio" name="type" id="check_team" value="2" @if($record->type == 2) checked @endif style="width:22px;height:22px!important;" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif><span style="margin-bottom: 13px;position: absolute;left: 50px;">{{trans('base.GROUP_PROJECT')}}</span></label>
                                </div>
                            </div>
                        </div>
                        @if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0)
                        <div class="form-group">
                            @if($record->status == 0)
                            <button class="btn btn-submit" type="submit" name="draft" value="1" style="background:#fd7700;">{{trans('base.Save')}}</button>
                            @endif
                            <button class="btn btn-submit" type="submit" name="draft" value="0" style="background:#fd7700;">@if($record->status != 0) <i class="icon-pencil" style="font-size: 24px;color: #333;"></i>{{trans('base.Edits')}} @else <img src="/public/assets2/img/send.png">  {{trans('base.SEND')}} @endif</button>
                            <a href="{{route('frontend.project.index')}}" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;color:black">{{trans('base.Back')}}</a>
                            <!--<a href="#" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;"><img style="width:24px;" src="/public/assets2/img/trash.png"></a>-->
                        </div>
                        @if($record->status != 0)
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-left"><span class="orange">{{trans('base.Reason_for_return')}}: </label>
                            <div class="col-md-12">
                                <textarea class="form-control" readonly="">{{$record->reason}}</textarea>
                            </div>
                        </div>
                        @endif
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script src="{!! asset('assets/global_assets/js/plugins/forms/selects/select2.min.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script src="{!! asset('ckfinder/ckfinder.js') !!}"></script>
<script>
    $('#check_member').change(function(){
        if($(this).is(':checked')){
            $('.choose-member').prop('disabled', false);
        }else{
             $('.choose-member').prop('disabled', 'disabled');
        }
    })
    $('#check_level').change(function(){
        if($(this).is(':checked')){
            $('.choose-level').prop('disabled', false);
        }else{
             $('.choose-level').prop('disabled', 'disabled');
        }
    })
    $.ajaxSetup ({
        cache: false
    });
    $('.load-page').click(function(){
        $('.load-page').removeClass('active');
        $(this).addClass('active');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
        $(".project-content").html(ajax_load).load($(this).data('href')); 
    })
    ClassicEditor
            .create( document.querySelector( '#before_content' ) )
            .then(editor => {
                  console.log(editor);
                  descriptionEditor = editor;
                  if($('#before_content').hasClass('disabled')){
                        descriptionEditor.isReadOnly = true;
                  }
                })
            .catch( error => {
                console.error( error );
    } );
    ClassicEditor
            .create( document.querySelector( '#after_content' ) )
            .then(editor => {
                  console.log(editor);
                  descriptionEditor = editor;
                  if($('#after_content').hasClass('disabled')){
                        descriptionEditor.isReadOnly = true;
                  }
                })
            .catch( error => {
                console.error( error );
    } );
    
</script>
@stop