@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
    <div class="row" style="margin:0px;">
        @include('frontend.project.sidebar')
        <div class="col-md-10">
            <div class="content project-content">
                <div class='title-project'>
                    <h4 style="margin-top:25px">{{trans('base.IMPORT_NEW_IMPROVED_PROJECTS_INFORMATION')}}</h4>
                    <a class='helper-question' href="javascript:void(0)" style="color:#333;position: absolute;right: 16px;top: 32px; font-size: 22px;}"><img src='{{asset('/img/information.png')}}' style='width: 27px;margin-bottom: 6px;margin-right: 8px;'>Hỏi đáp</a>
                </div>
                <div class="form-create">
                    <form method="POST" action="{{route('frontend.project.store')}}" id="FrmCreateProject">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom: 30px;">
                            <div class="notification-member col-md-6">
                                <div class="row" id='info_member' style="border: 1px solid;margin-left: 0px;border-radius: 4px;">
                                    <div class="col-md-3" style='padding-left: 0px'>
                                        <div class="img-member" style="line-height:162px;">
                                            <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {!!\Auth::guard('member')->user()->avatar!!} @endif" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <h3>{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                        <p>{{trans('base.Employee_code')}}: <span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                        <p>{{trans('base.Position')}}: <span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                        <p>{{trans('base.Part')}}: <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                        <p>Team: <span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="check_member" style="width:22px;height:22px;">
                                    <label class="form-check-label" for="check_member" style="margin: 4px 0px 0px 14px;">{{trans('base.Where_to_enter_for_someone_else')}}</label>
                                </div>
                                <div class="form-group row" style="margin-top: 25px;">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">{{trans('base.Select_employee_code')}}</label>
                                    <div class="col-sm-6">
                                        <select class="form-control select-search choose-member" name="member_id" data-placeholder="{{trans('base.Select_employee_code')}}" disabled>
                                            {!!$member_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">{{trans('base.SUBJECT_NAME')}}</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="name" type="text" required=""> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.BEFORE')}}</span> {{trans('base.IMPROVE')}}: </label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="before_content" name="before_content">{!!old('before_content')!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" class="upload-images" multiple="multiple" name="before_upload[]" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview">
                                        <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="before_images" class="image_data">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.AFTER')}}</span> {{trans('base.IMPROVE')}}: </label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="after_content" name="after_content">{!!old('after_content')!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" class="upload-images" multiple="multiple" name="after_upload[]" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                         <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                         
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="after_images" class="image_data">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-3" style="margin-left: 16px;">
                                <input type="checkbox" class="form-check-input" id="check_level" style="width:22px;height:22px;">
                                <label class="form-check-label" for="check_level" style="margin: 4px 0px 0px 14px;">{{trans('base.CONSIDER_HIGH_LEVEL_PROJECTS')}}</label>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control select-search choose-level" name="level" data-placeholder="{{trans('base.Choose_level')}}" disabled="">
                                    {!!$level_html!!}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_personal"><input type="radio" name="type" id="check_personal" value="1" checked style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">{{trans('base.PERSONAL_PROJECT')}}</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_team"><input type="radio" name="type" id="check_team" value="2" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">{{trans('base.GROUP_PROJECT')}}</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-submit submit-form" type="button" style="background:#fd7700;"><img src="/public/assets2/img/send.png">  {{trans('base.SEND')}}</button>
                            <button class="btn btn-submit" name='draft' value='1' style="background: #fff;border:1px solid #a6a5a5;" title="{{trans('base.Draft')}}"><img style="width:24px;" src="/public/assets2/img/documents.png"></button>
                            <a href="{{route('frontend.project.index')}}" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;color:black">{{trans('base.Back')}}</a>
                    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<div class="overlay"></div>
<div class="modal fade" id="modal_helper_question" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel"><span>Hỏi đáp</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!!\App\Config::first()->content!!}
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets/global_assets/js/plugins/forms/selects/select2.min.js') !!}"></script>
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script>
    $('#check_member').change(function(){
        if($(this).is(':checked')){
            $('.choose-member').prop('disabled', false);
        }else{
             $('.choose-member').prop('disabled', 'disabled');
        }
    })
    $('#check_level').change(function(){
        if($(this).is(':checked')){
            $('.choose-level').prop('disabled', false);
        }else{
             $('.choose-level').prop('disabled', 'disabled');
        }
    })
    $('.choose-member').change(function(){
        var member_id = $(this).val();
        $.ajax({
            url:'/api/getInfoMember',
            method:'POST',
            data:{member_id:member_id},
            success: function(response){
                $('#info_member').html(response.html);
            }
        })
    })
    $.ajaxSetup ({
        cache: false
    });
    $('.load-page').click(function(){
        $('.load-page').removeClass('active');
        $(this).addClass('active');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
        $(".project-content").html(ajax_load).load($(this).data('href')); 
    })
    ClassicEditor
            .create( document.querySelector( '#before_content' ) )
            .then( editor => {
                before_content = editor; // Save for later use.
            } )
            .catch( error => {
                console.error( error );
    } );
    ClassicEditor
            .create( document.querySelector( '#after_content' ) )
            .then( editor => {
                after_content = editor; // Save for later use.
            } )
            .catch( error => {
                console.error( error );
    } );
    $('.submit-form').click(function(e){
        e.preventDefault();
        if(before_content.getData() == '' || after_content.getData() == '' || $('input[name="name"]').val() == ''){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Nhập đầy đủ thông tin trước khi gửi");
            notification.push();
        }else{
            $('#FrmCreateProject').submit();
        }
    })
    $('.helper-question').click(function(){
        $('#modal_helper_question').modal('show');
    })
</script>
@stop