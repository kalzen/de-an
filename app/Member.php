<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;
    protected $table = 'member';
    protected $fillable = ['device_token','login_id','part_id', 'password','status','full_name','groups_id','department_id','team_id','level','note','avatar','is_deleted','position_id','email'];
    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }
    const LEVEL_ARR = [['id'=>1,'name'=>1],['id'=>2,'name'=>2],['id'=>3,'name'=>3],['id'=>4,'name'=>4],['id'=>5,'name'=>5]];
    const LEVEL_1 = 1;
    const LEVEL_2 = 2;
    const LEVEL_3 = 3;
    const LEVEL_ADMIN = 4;
    const LEVEL_5 = 5;
    public function part(){
        return $this->belongsTo('\App\Part');
    }
    public function department(){
        return $this->belongsTo('\App\Department');
    }
    public function groups(){
       return $this->belongsTo('\App\Groups');
    }
    public function team(){
        return $this->belongsTo('\App\Team');
    }
    public function position(){
        return $this->belongsTo('\App\Position');
    }
    public function project(){
        return $this->hasMany('\App\Project');
    }
     public function schedule() {
        return $this->belongsToMany('\App\Schedule', 'member_schedule', 'member_id', 'schedule_id');
    }
    public function seen() {
        return $this->belongsToMany('\App\Schedule', 'seen_schedule', 'member_id', 'schedule_id');
    }
}
