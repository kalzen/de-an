<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProjectRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Repositories\LevelRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller {
    public function __construct(MemberRepository $memberRepo,ProjectRepository $projectRepo,LevelRepository $levelRepo) {
        $this->projectRepo = $projectRepo;
        $this->memberRepo = $memberRepo;
        $this->levelRepo= $levelRepo;
    }
    public function index(){
        Session::put('p_page',1);
        if(isset($_GET['keyword'])){
            $records = $this->projectRepo->fillter($_GET['keyword'],15);
            $keyword = $_GET['keyword'];
        }else{
            $records = $this->projectRepo->getIndex(15);
            $keyword='';
            Session::forget('keyword');
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/index', compact('records','keyword'));
        } else {
            return view('frontend/project/index', compact('records','keyword'));
        }
    }
    public function listProject(Request $request){
        $records = $this->projectRepo->getIndexByMember($request);
        if($request->get('department_id')){
            $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get(),$request->get('department_id'));
        }else{
            $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get());
        }
        if($request->get('level')){
            if($request->get('level') == 'KCD'){
                $level_html = '<option></option><option value="KCD" selected>Không cấp độ</option>';
            }else{
                $level_html = '<option></option><option value="KCD">Không cấp độ</option>';
            }
            $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get(),$request->get('level'));
        }else{
            $level_html = '<option></option><option value="KCD">Không cấp độ</option>';
            $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get());  
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/list', compact('records','department_html','level_html'));
        } else {
            return view('frontend/project/list', compact('records','department_html','level_html'));
        }
    }
    public function create() {
        $members = \App\Member::get();
        foreach($members as $key=>$member){
            $member->name = $member->login_id;
        }
        $member_html = \App\Helpers\StringHelper::getSelectOptions($members);
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all());
        if (config('global.device') != 'pc') {
            return view('mobile/project/create', compact('member_html','level_html'));
        } else {
            return view('frontend/project/create', compact('member_html','level_html'));
        }
    }
    public function store(Request $request) {
        $input = $request->all();
        if(!isset($input['member_id'])){
            $input['member_id']= \Auth::guard('member')->user()->id;
        }
        $member = $this->memberRepo->find($input['member_id']);
        if(isset($input['draft'])){
            $input['status'] = 0;
        }else{
            $input['status'] = $member->level;
        }
        $project = $this->projectRepo->create($input);
        if($project->status > 0 && $project->type != \App\Project::TYPE_TEAM){
            $this->memberRepo->Notificate($this->memberRepo->getLevelNext($member->department_id,$member->level), ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
        }
        if ($project->id) {
            return redirect()->route('frontend.project.index')->with('success', 'Tạo mới thành công');
        } else {
            return redirect()->route('frontend.project.index')->with('error', 'Tạo mới thất bại');
        }
    }
    public function edit($id) {
        $record = $this->projectRepo->find($id);
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all(),$record->level);
        if (config('global.device') != 'pc') {
            return view('mobile/project/update', compact('record','level_html'));
        } else {
            return view('frontend/project/update', compact('record','level_html'));
        }
    }
    public function update(Request $request, $id) {
        $input = $request->all();
        if($input['draft'] == 0){
            $input['status'] = \Auth::guard('member')->user()->level;
            $input['created_at'] = date('Y-m-d H:i:s');
        }
        
        $res = $this->projectRepo->update($input, $id);
        if ($res) {
            return redirect()->route('frontend.project.index')->with('success', 'Cập nhật thành công');
        } else {
            return redirect()->route('frontend.project.index')->with('error', 'Cập nhật thất bại');
        }
    }
    public function fillter($keyword){
        Session::put('p_page',1);
        $records = $this->projectRepo->fillter($keyword,10);
        return view('frontend/project/fillter',compact('records'));
    }
    public function view($id){
        $record = $this->projectRepo->find($id);
        $member = $this->memberRepo->find($record->member_id);
        $number_of_reviews = DB::table('member')->where('department_id',$member->department_id)->where('level','>=',$member->level)->where('level','<',\App\Member::LEVEL_ADMIN)->count(DB::raw('DISTINCT level')) -1;
        for($i=1;$i < $number_of_reviews + 1;$i++){
            if($i == ($number_of_reviews) && $number_of_reviews < \App\Project::STATUS_ACTIVE -1){
                $level =  \App\Project::STATUS_ACTIVE;
            }else{
                $level =  $record->member->level + $i;
            }
            $logapproved[$i] = \App\LogApproved::where('project_id',$record->id)->where('level',$level)->first();
            if($logapproved[$i]){
                $logapproved[$i]->count = \App\LogApproved::where('project_id',$record->id)->where('level',$level)->count();
            }
        }
        if(!isset($logapproved)){
            $logapproved = null;
        }
        $level = $this->memberRepo->getLevelPrev(\Auth::guard('member')->user()->department_id,\Auth::guard('member')->user()->level);
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all(),$record->level);
        if (config('global.device') != 'pc') {
            return view('mobile/project/view', compact('record','logapproved','level_html','level'));
        } else {
            return view('frontend/project/view', compact('record','logapproved','level_html','level'));
        }
    }
    public function chart(){
        $label1 = [];
        $data1=[];
        $label2 = [];
        $data2=[];
        for($i=6;$i--;$i>0){
            $dt = Carbon::now();
            $dt->subMonth($i);
            $month = $dt->month;
            $year = $dt->year;
            $label1[] = "Tháng ".$dt->month;
            $data1[] = \App\Project::whereMonth('created_at',$month)->whereYear('created_at',$year)->where('is_destroy',0)->where('is_deleted',0)->where('status','>',0)->count();
        }
        $status_pending = \App\Project::whereIn('status',[1,2])->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_approved = \App\Project::where('status','=',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_return = \App\Project::where('status','=',\App\Project::STATUS_CANCEL)->where('is_deleted',0)->where('is_destroy',0)->count();
        for($j=0;$j<11;$j++){
            if($j == 0){
                $label2[] = 'Không cấp độ';
                $data2[]= \App\Project::where('level',null)->where('is_destroy',0)->where('is_deleted',0)->where('status','>',0)->count();
            }else{
                $label2[] = 'Cấp '.$j;
                $data2[]=\App\Project::where('level',$j)->where('is_destroy',0)->where('is_deleted',0)->where('status','>',0)->count();
            }
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/chart');
        }else{
            return view('frontend/project/chart',compact('label1','data1','status_pending','status_approved','status_return','label2','data2'));
        }
    }
    
}
