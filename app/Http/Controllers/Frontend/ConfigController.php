<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ConfigRepository;

class ConfigController extends Controller
{
    public function __construct(ConfigRepository $configRepo) {
        $this->configRepo= $configRepo;
    }

    public function index(){
        $record = $this->configRepo->find(1);
        if (config('global.device') != 'pc') {
            return view('mobile/config/index', compact('record'));
        } else {
            return view('frontend/config/index', compact('record'));
        }
    }
    public function update(Request $request){
        $input = $request->all();
        $record = $this->configRepo->update($input,1);
        return redirect()->route('frontend.config.index')->with('success', 'Cập nhật thành công');
    }
}
