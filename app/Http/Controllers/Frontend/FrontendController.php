<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Repositories\SlideRepository;

class FrontendController extends Controller {

    public function __construct(SlideRepository $slideRepo) {
        $this->slideRepo = $slideRepo;
    }

    public function index() {
        $slides = $this->slideRepo->all();
        $documents = \App\Document::where('status',1)->orderBy('created_at','desc')->take(3)->get();
        if(strtotime(date('Y-m-24')) > strtotime(date('Y-m-d'))){
            $project_month = DB::table('project')
                   ->where('created_at','<',date('Y-m-24'))->where('created_at','>',date('Y-m-23',strtotime("-1 months")))->where('status', \App\Project::STATUS_ACTIVE)->where('is_deleted',0);
        }else{
            $project_month = DB::table('project')
                   ->where('created_at','>',date('Y-m-24'))->where('created_at','<',date('Y-m-d',strtotime("+1 months")))->where('status', \App\Project::STATUS_ACTIVE)->where('is_deleted',0);  
        }
        $project_year = DB::table('project')
                   ->whereYear('created_at',date('Y'))->where('status', \App\Project::STATUS_ACTIVE)->where('is_deleted',0);
        $rank_team = DB::table('member')->joinSub($project_month, 'project_month', function ($join) {
                    $join->on('member.id', '=', 'project_month.member_id');
                })->join('team','member.team_id','=','team.id')
                ->select('team.name',DB::raw('count(project_month.id) as count'))
                ->groupBy('team.name')
                ->orderBy('count','DESC')->take(3)->get();  
        $rank_month = DB::table('member')
            ->joinSub($project_month, 'project_month', function ($join) {
                $join->on('member.id', '=', 'project_month.member_id');
            })->select('member.id',DB::raw('count(project_month.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();
        $rank_year = DB::table('member')
            ->joinSub($project_year, 'project_year', function ($join) {
                $join->on('member.id', '=', 'project_year.member_id');
            })->select('member.id',DB::raw('count(project_year.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();    
        if (config('global.device') != 'pc') {
            return view('mobile/home/index',compact('rank_month','rank_year','slides','documents','rank_team'));
        } else {
            return view('frontend/home/index',compact('rank_month','rank_year','slides','documents','rank_team'));
        }
    }
    public function home(){
        return view('frontend/home/home');
    }
    public function create(){
        return view('frontend/home/create');
    }
    public function view(){
        if (config('global.device') != 'pc') {
            return view('mobile/home/view');
        } else {
            return view('frontend/home/view');
        }
    }
    public function changeLanguage($locale){
        if (in_array($locale, \Config::get('app.locales'))) {
          session(['locale' => $locale]);
        }
        return redirect()->back();
    }

}
