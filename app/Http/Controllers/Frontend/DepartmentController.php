<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    public function index(){
        $records = \App\Department::all();
        if (config('global.device') != 'pc') {
            return view('mobile/department/index', compact('records'));
        } else {
            return view('frontend/department/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Department::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
