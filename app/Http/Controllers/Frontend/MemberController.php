<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class MemberController extends Controller
{
    public function __construct(MemberRepository $memberRepo) {
        $this->memberRepo = $memberRepo;
    }

    public function index() {
        Session::put('page',1);
        Session::forget('search');
        $records = $this->memberRepo->getIndex(10);
        $a = Carbon::now();
        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where('created_at','<=',date('Y-m-d'))->where('created_at','>=',date('Y-m-d',strtotime("-1 months")))->count();
            $record->total_project_month = \App\Project::where('member_id',$record->id)->where('status','<>',0)->where('is_deleted',0)->where('created_at','<=',date('Y-m-d'))->where('created_at','>=',date('Y-m-d',strtotime("-1 months")))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
            $record->total_project_quarter = \App\Project::where('member_id',$record->id)->where('status','<>',0)->where('is_deleted',0)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
            $record->count_project = \App\Project::where('member_id',$record->id)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->count();
            $record->total_count_project = \App\Project::where('member_id',$record->id)->where('status','<>',0)->where('is_deleted',0)->count();
        }
        $part_html = \App\Helpers\StringHelper::getSelectHtml(\App\Part::get());
        $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get());
        $groups_html = \App\Helpers\StringHelper::getSelectHtml(\App\Groups::get());
        $team_html = \App\Helpers\StringHelper::getSelectHtml(\App\Team::get());
        $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get());
        if (config('global.device') != 'pc') {
            return view('mobile/member/index', compact('records','part_html','department_html','groups_html','team_html','position_html'));
        } else {
            return view('frontend/member/index', compact('records','part_html','department_html','groups_html','team_html','position_html'));
        }
    }
    
    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->memberRepo->validateCreate());
        $input['password'] = bcrypt($input['password']);
        $input['avatar'] = '/public/upload/images/Default_avatar_VHE.png';
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $member = $this->memberRepo->create($input);
        if ($member) {
            return redirect()->route('frontend.member.index')->with('success', 'Tạo mới thành công');
        } else {
            return redirect()->route('frontend.member.index')->with('error', 'Tạo mới thất bại');
        }
    }
    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->memberRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $res = $this->memberRepo->update($input, $id);
        return redirect()->route('frontend.member.index')->with('success', 'Cập nhật thành công');
    }
}
