<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LevelController extends Controller
{
     public function index(){
        $records = \App\Level::all();
        if (config('global.device') != 'pc') {
            return view('mobile/level/index', compact('records'));
        } else {
            return view('frontend/level/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Level::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
