<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index(){
        $records = \App\Document ::all();
        if (config('global.device') != 'pc') {
            return view('mobile/document/index', compact('records'));
        } else {
            return view('frontend/document/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Document::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
