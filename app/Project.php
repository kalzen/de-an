<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    //
    protected $table = "project";
    protected $fillable = [
        'name', 'member_id', 'before_content', 'after_content', 'status','level','is_deleted','is_saved','reason','before_images','after_images','type','is_destroy','created_at'
    ]; 
    const STATUS_LEVEL_2 = 1;
    const STATUS_LEVEL_3 = 2;
    const STATUS_ACTIVE = 3;
    const STATUS_INTIVE = 4;
    const STATUS_CANCEL = 5;
    const TYPE_PERSONAL = 1;
    const TYPE_TEAM = 2;
    public function member() {
        return $this->belongsTo('\App\Member');
    }
    public function created_at(){
        return date('d/m/Y',strtotime($this->created_at));
    }
    public function logapproved(){
        return $this->hasMany('\App\LogApproved');
    }
    public function levels(){
        return $this->belongsTo('\App\Level','level','id');
    }
    public function before_images_arr(){
        return explode(',',$this->before_images);
    }
     public function after_images_arr(){
        return explode(',',$this->after_images);
    }
}
