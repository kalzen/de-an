<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class LogApprovedRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\LogApproved';
    }
    public function deleteProject($project_id){
        return $this->model->whereIn('project_id',$project_id)->delete();
    }
}
