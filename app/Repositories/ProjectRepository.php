<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class ProjectRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Project';
    }

    public function validateCreate() {
        return $rules = [
            'title' => 'required|unique:project',
            'alias' => 'required',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'title' => 'required|unique:project,title,' . $id . ',id',
            'alias' => 'required',
        ];
    }
    public function getByMember($member_id){
        return $this->model->where('member_id',$member_id)->get();
    }
    public function getIndex($limit){
        $query = $this->model->where('is_deleted',0);
        $start = (Session::get('p_page')-1) * $limit;
        Session::put('_p_count',count($query->where('member_id',\Auth::guard('member')->user()->id)->get()));
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        $data = $query->where('member_id',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;   
    }
    public function getAll(){
        $data = $this->model->get();
        return $data;
        
    }
    public function getList($limit){
        $start = (Session::get('p_page')-1) * $limit;
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if((Session::get('keyword'))){
            $keyword = Session::get('keyword');
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
            }else{
                $query = $query->where('is_deleted',0);
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
            }
        }
        Session::put('_p_count',count($query->get()));
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;   
    }
    public function getListProject($limit,$request){
        $start = (Session::get('list_page')-1) * $limit;
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if((Session::get('keyword_project'))){
            $keyword = Session::get('keyword_project');
            if($keyword == 'all'){
                Session::forget('keyword_project');
                Session::forget('search');
                Session::forget('month');
                Session::forget('year');
            }
            if($keyword == 'pending'){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $member = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->orderBy('level','DESC')->first();
                $query = $query->where('status',$member->level)->whereIn('member_id',$ids);
                Session::put('keyword_project','pending');
            }
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
                Session::put('keyword_project','save');
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
                Session::put('keyword_project','remove');
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
                 Session::put('keyword_project','draft');
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
                Session::put('keyword_project','send');
            }
            if($keyword == 'approved'){
                $query = $query->where('status','=',\App\Project::STATUS_ACTIVE);
                Session::put('keyword_project','approved');
            }
            if($keyword == 'return'){
                $query = $query->where('status','=',\App\Project::STATUS_CANCEL);
                Session::put('keyword_project','return');
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
                Session::put('keyword_project','save_member');
            }
        }
        if(Session::get('search')){
            $search = Session::get('search');
            if(isset($search['full_name'])){
            $member_ids = \App\Member::where('full_name','like','%'.$search['full_name'].'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
            }
            if(isset($search['department_id'])){
                $member_ids = \App\Member::where('department_id',$search['department_id'])->get()->pluck('id')->toArray();
                $query = $query->whereIn('member_id',$member_ids);
            }
            if(isset($search['level'])){
                if($search['level'] == 0){
                    $query = $query->where('level',NULL);
                }else{
                    $query = $query->where('level',$search['level']);
                }
            }
            if(isset($search['status'])){
                $search['status'] = explode(',',$search['status']);
                $query = $query->whereIn('status',$search['status']);
            }
        }
        
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        if($request->get('month') && $request->get('year')){
            $query = $query->whereMonth('created_at',$request->get('month'))->whereYear('created_at',$request->get('year'));
            Session::put('month',$request->get('month'));
            Session::put('year',$request->get('year'));
        }
        Session::put('_list_count',count($query->get()));
        if((Session::get('list_page') * $limit) > Session::get('_list_count')){
            Session::put('_list_pages',Session::get('_list_count'));
        }else{
            Session::put('_list_pages',Session::get('list_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->get();
        return $data;   
    }
    public function save($project_id){
        return $this->model->whereIn('id',$project_id)->update(['is_saved'=>1]);
    }
    public function send($project_id,$level){
        return $this->model->whereIn('id',$project_id)->update(['status'=>$level]);
    }
    public function remove($project_id){
        return $this->model->whereIn('id',$project_id)->update(['is_deleted'=>1]);
    }
    public function fillter($keyword,$limit){
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if($keyword == 'save'){
            $query = $query->where('is_saved',1);
            Session::put('keyword','save');
        }
        if($keyword == 'remove'){
            $query = $query->where('is_deleted',1);
            Session::put('keyword','remove');
        }else{
            $query = $query->where('is_deleted',0);
        }
        if($keyword == 'draft'){
            $query = $query->where('status',0);
            Session::put('keyword','draft');
        }
        if($keyword == 'send'){
            $query = $query->where('status','>',0);
            Session::put('keyword','send');
        }
        $start = (Session::get('p_page')-1) * $limit;
        Session::put('_p_count',count($query->get()));
        $data = $query->orderBy('created_at','DESC')->get();
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        return $data;
    }
    public function getIndexByMember($request){
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if(!is_null($request->get('keyword_project'))){
            $keyword = $request->get('keyword_project');
            if($keyword == 'pending' && \Auth::guard('member')->user()->level != 5){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $member = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->orderBy('level','DESC')->first();
                $query = $query->where('status',$member->level)->whereIn('member_id',$ids);
            }elseif($keyword == 'pending' && \Auth::guard('member')->user()->level == 5){
                $query = $query->whereIn('status',[1,2]);
            }
            if($keyword == 'return'){
                $query = $query->where('status', \App\Project::STATUS_CANCEL);
            }
            if($keyword == 'approved'){
                $query = $query->where('status',\App\Project::STATUS_ACTIVE);
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
            }
        }
        if($request->get('full_name')){
            $member_ids = \App\Member::where('full_name','like','%'.$request->get('full_name').'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if($request->get('department_id')){
            $member_ids = \App\Member::where('department_id',$request->get('department_id'))->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if($request->get('level')){
            if($request->get('level') == 'KCD'){
                $query = $query->where('level',NULL);
            }else{
                $query = $query->where('level',$request->get('level'));
            }
        }
        if($request->get('status')){
            $search['status'] = explode(',',$request->get('status'));
            $query = $query->whereIn('status',$search['status']);
        }
        $keyword = $request->get('keyword');
        if($keyword == 'unapproved'){
            $query = $query->where('status','<',\App\Project::STATUS_ACTIVE)->whereDate('created_at',date('Y-m-d'));
        }
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        $data = $query->orderBy('created_at','DESC')->get();
        return $data;  
    }
    
     public function export($ids){
        return $this->model->whereIn('id',$ids)->get();
    }
    public function getExport(){
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if(isset($_GET['keyword_project'])){
            $keyword = $_GET['keyword_project'];
            if($keyword == 'pending'){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $member = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->orderBy('level','DESC')->first();
                $query = $query->where('status',$member->level)->whereIn('member_id',$ids);
            }
            if($keyword == 'return'){
                $query = $query->where('status', \App\Project::STATUS_CANCEL);
            }
            if($keyword == 'approved'){
                $query = $query->where('status',\App\Project::STATUS_ACTIVE);
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
            }
        }
        if(isset($_GET['full_name']) && $_GET['full_name'] != ''){
            $member_ids = \App\Member::where('full_name','like','%'.$_GET['full_name'].'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if(isset($_GET['department_id']) && $_GET['department_id'] != ''){
            $member_ids = \App\Member::where('department_id',$_GET['department_id'])->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if(isset($_GET['level']) && $_GET['level'] != ''){
            if($_GET['level'] == 'KCD'){
                $query = $query->where('level',NULL);
            }else{
                $query = $query->where('level',$_GET['level']);
            }
        }
        if(isset($_GET['status']) && $_GET['status'] != ''){
            $search['status'] = explode(',',$_GET['status']);
            $query = $query->whereIn('status',$search['status']);
        }
        if(isset($_GET['keyword']) && $_GET['keyword'] == 'unapproved'){
            $query = $query->where('status','<',\App\Project::STATUS_ACTIVE)->whereDate('created_at',date('Y-m-d'));
        }
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        $data = $query->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function checkDraft($project_ids){
        return $this->model->whereIn('id',$project_ids)->where('status','<>',0)->count();
    }
    function htmlToPlainText($str){
        $str = html_entity_decode($str, ENT_QUOTES | ENT_XML1, 'UTF-8');
        $str = htmlspecialchars_decode($str);
        $str = html_entity_decode($str);
        $str = strip_tags($str);
        return $str;
    }
    public function exportForm1($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $department_id = $request->get('department_id');
        $member_ids = $this->model->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->groupBy('member_id')->pluck('member_id')->toArray();
        if(is_null($department_id)){
            $records = \App\Member::whereNotIn('id',$member_ids)->where('level','<',4)->where('department_id','<>',8)->get();
        }else{
            $records = \App\Member::whereNotIn('id',$member_ids)->where('level','<',4)->where('department_id','<>',8)->where('department_id',$department_id)->get();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DANH SÁCH CÁ NHÂN KHÔNG CÓ ĐỀ TÀI THÁNG   NĂM')->mergeCells("A3:F3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('E4', 'Ngày:')->mergeCells("E4:F4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Mã số nhân viên')
            ->setCellValue('E5', 'Họ và tên')
            ->setCellValue('F5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:F5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->department->name)
                ->setCellValue('C' . $rows, $record->team ? $record->team->name : '')
                ->setCellValue('D' . $rows, $record->login_id)
                ->setCellValue('E' . $rows, $record->full_name)
                ->setCellValue('F' . $rows, $record->note) 
                ;
            $rows++;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."F" . $rows . "")->applyFromArray($styleArray);
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."C" . $rows . "")
                    ->setCellValue('D' . $rows, 'Người phê duyệt')->mergeCells("D". $rows .":"."F" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:F3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."C" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("D". $rows .":"."F" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DANH_SACH_CA_NHAN_KHONG_CO_DE_TAI_NGAY_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DANH_SACH_CA_NHAN_KHONG_CO DE_TAI_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DANH_SACH_CA_NHAN_KHONG_CO DE_TAI_' . $timestamp . '.xls';
        return $href;
    }
    public function exportForm2($request){
        $teams = \App\Team::get();
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        foreach($teams as $key=>$team){
            $count_project = 0;
            foreach($team->member as $k=>$member){
                $count_project += $member->project()->where('status',\App\Project::STATUS_ACTIVE)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->count();
            }
            $team->total = $count_project;
            $team->rank =0;
            $team->count_member = count($team->member);
            $team->department_name = $team->member()->first()->department ? $team->member()->first()->department->name : ''; 
        }
        $records = collect($teams->toArray())->sortByDesc('total')->values();
        $rank  = 1;
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DANH SÁCH XẾP LOẠI NHÓM THÁNG   NĂM')->mergeCells("A3:H3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('G4', 'Ngày:')->mergeCells("G4:H4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Tổng số người')
            ->setCellValue('E5', 'Số đề tài')
            ->setCellValue('F5', 'Tỉ lệ đề tài/ người')
            ->setCellValue('G5', 'Xếp hạng')
            ->setCellValue('H5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:H5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        $no = 1;
        $total_member = 0;
        $total_project = 0;
        foreach ($records as $key=>$record)
        {
            $total_member += $record['count_member'] ;
            $total_project += $record['total'] ;
            if($key == 0){
                $rank = 1;
            }else{
                if($records[$key - 1]['total'] > $record['total']){
                    $rank ++;
                }
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record['department_name'])
                ->setCellValue('C' . $rows, $record['name'])
                ->setCellValue('D' . $rows, $record['count_member'])
                ->setCellValue('E' . $rows, $record['total'])
                ->setCellValue('F' . $rows, $record['total']/$record['count_member'])
                ->setCellValue('G' . $rows, $rank)
                ->setCellValue('H' . $rows, '') 
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A'. $rows, 'Tổng')->mergeCells("A". $rows .":"."C" . $rows . "");
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."C" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()
                    ->setCellValue('D'. $rows, $total_member)
                    ->setCellValue('E'. $rows, $total_project)
                    ->setCellValue('F'. $rows, $total_project/$total_member);
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
        $rows = $rows +2;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."D" . $rows . "")
                    ->setCellValue('E' . $rows, 'Người phê duyệt')->mergeCells("E". $rows .":"."H" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."D" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("E". $rows .":"."H" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DANH_SACH_XEP_LOAI_NHOM_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DANH_SACH_XEP_LOAI_NHOM_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DANH_SACH_XEP_LOAI_NHOM_' . $timestamp . '.xls';
        return $href;
       
    }
    public function exportForm3($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $department_id = $request->get('department_id');
        if(is_null($department_id)){
            $records = \App\Member::where('level','<',4)->get();
        }else{
            $records = \App\Member::where('level','<',4)->where('department_id',$department_id)->get();
        }
        foreach($records as $key=>$record){
            $record->department_name = $record->department ? $record->department->name : '';
            $record->team_name = $record->team ? $record->team->name : '';
            $record->count_project = $record->project()->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->count();
        }
        $records = collect($records->toArray())->sortByDesc('count_project')->values();
        $rank  = 1;
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DANH SÁCH XẾP LOẠI CÁ NHÂN QUÝ   NĂM')->mergeCells("A3:H3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('G4', 'Ngày:')->mergeCells("G4:H4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Mã số nhân viên')
            ->setCellValue('E5', 'Họ và tên')
            ->setCellValue('F5', 'Số đề tài')
            ->setCellValue('G5', 'Xếp hạng')
            ->setCellValue('H5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:H5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        foreach ($records as $key=>$record)
        {
            
            if($key == 0){
                $rank = 1;
            }else{
                if($records[$key - 1]['count_project'] > $record['count_project']){
                    ++$rank;
                }
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record['department_name'])
                ->setCellValue('C' . $rows, $record['team_name'])
                ->setCellValue('D' . $rows, $record['login_id'])
                ->setCellValue('E' . $rows, $record['full_name'])
                ->setCellValue('F' . $rows, $record['count_project'])
                ->setCellValue('G' . $rows, $rank)
                ->setCellValue('H' . $rows, '') 
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."D" . $rows . "")
                    ->setCellValue('E' . $rows, 'Người phê duyệt')->mergeCells("E". $rows .":"."H" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."D" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("E". $rows .":"."H" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DANH_SACH_XEP_LOAI_CA_NHAN_QUY_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DANH_SACH_XEP_LOAI_CA_NHAN_QUY_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DANH_SACH_XEP_LOAI_CA_NHAN_QUY_' . $timestamp . '.xls';
        return $href;
    }
    public function exportForm4($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $department_id = $request->get('department_id');
        if(is_null($department_id)){
            $records = \App\Project::where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
        }else{
            $member_ids = \App\Member::where('department_id',$department_id)->pluck('id')->toArray();
            $records = \App\Project::whereIn('member_id',$member_ids)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'TỔNG HỢP ĐỀ TÀI THÁNG   NĂM')->mergeCells("A3:J3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('I4', 'Ngày:')->mergeCells("I4:J4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(100);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(100);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Mã số nhân viên')
            ->setCellValue('E5', 'Họ và tên')
            ->setCellValue('F5', 'Cấp độ đề tài')
            ->setCellValue('G5', 'Tên đề tài')
            ->setCellValue('H5', 'Trước cải tiến')
            ->setCellValue('I5', 'Sau cải tiến')
            ->setCellValue('J5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:J5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5','I5','J5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        foreach ($records as $key=>$record)
        {
            if($record->level > 0){
                $level = 'Cấp độ '.$record->level; 
            }else{
                $level = 'Không cấp độ';
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->member->department ? $record->member->department->name : '')
                ->setCellValue('C' . $rows, $record->member->team ? $record->member->team->name : '')
                ->setCellValue('D' . $rows, $record->member->login_id)
                ->setCellValue('E' . $rows, $record->member->full_name)
                ->setCellValue('F' . $rows, $level)
                ->setCellValue('G' . $rows, $record->name)
                ->setCellValue('H' . $rows, $this->htmlToPlainText($record->before_content))
                ->setCellValue('I' . $rows, $this->htmlToPlainText($record->after_content))
                ->setCellValue('J' . $rows, '') 
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."J" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."E" . $rows . "")
                    ->setCellValue('F' . $rows, 'Người phê duyệt')->mergeCells("F". $rows .":"."J" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."E" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("F". $rows .":"."J" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="TONG_HOP_DE_TAI_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/TONG_HOP_DE_TAI_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/TONG_HOP_DE_TAI_' . $timestamp . '.xls';
        return $href;
    }
}
