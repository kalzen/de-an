<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogProject extends Model
{
    protected $table = 'log_project';
    protected $fillable = ['member_id','project_id'];
}
