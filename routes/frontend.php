<?php

Route::group(['middleware' => ['frontend','locale']], function() {
    Route::get('/home', ['as' => 'home', 'uses' => 'Frontend\FrontendController@home']);
    Route::get('/create', ['as' => 'home.create', 'uses' => 'Frontend\FrontendController@create']);
    Route::get('/view', ['as' => 'home.view', 'uses' => 'Frontend\FrontendController@view']);\
    // Start route member
    Route::get('/member', ['as' => 'frontend.member.index', 'uses' => 'Frontend\MemberController@index']);
    Route::post('/member/stote', ['as' => 'frontend.member.store', 'uses' => 'Frontend\MemberController@store']);
    Route::post('/member/update/{id}', ['as' => 'frontend.member.update', 'uses' => 'Frontend\MemberController@update']);
    // end route member
    
    //Start route project
    Route::get('/project/list', ['as' => 'frontend.project.list', 'uses' => 'Frontend\ProjectController@listProject']);
    Route::get('/project/view/{id}', ['as' => 'frontend.project.view', 'uses' => 'Frontend\ProjectController@view']);
    Route::get('/project', ['as' => 'frontend.project.index', 'uses' => 'Frontend\ProjectController@index']);
    Route::get('/project/create', ['as' => 'frontend.project.create', 'uses' => 'Frontend\ProjectController@create']);
    Route::get('/project/edit/{id}', ['as' => 'frontend.project.edit', 'uses' => 'Frontend\ProjectController@edit']);
    Route::post('/project/store', ['as' => 'frontend.project.store', 'uses' => 'Frontend\ProjectController@store']);
    Route::post('/project/update/{id}', ['as' => 'frontend.project.update', 'uses' => 'Frontend\ProjectController@update']);
    Route::get('/project/fillter/{keyword}', ['as' => 'frontend.project.fillter', 'uses' => 'Frontend\ProjectController@fillter']); 
    Route::get('/project/chart', ['as' => 'frontend.project.chart', 'uses' => 'Frontend\ProjectController@chart']);
    //End route project
    
    //Start route part
    Route::get('/part', ['as' => 'frontend.part.index', 'uses' => 'Frontend\PartController@index']);
    Route::delete('/part/delete/{id}', ['as' => 'frontend.part.destroy', 'uses' => 'Frontend\PartController@destroy']);
    //End route part
    
     //Start route department
    Route::get('/department', ['as' => 'frontend.department.index', 'uses' => 'Frontend\DepartmentController@index']);
    Route::delete('/department/delete/{id}', ['as' => 'frontend.department.destroy', 'uses' => 'Frontend\DepartmentController@destroy']);
     //End route department
     
    //Start route groups
    Route::get('/groups', ['as' => 'frontend.groups.index', 'uses' => 'Frontend\GroupsController@index']);
    Route::delete('/groups/delete/{id}', ['as' => 'frontend.groups.destroy', 'uses' => 'Frontend\GroupsController@destroy']);
    //End route groups
    
    //Start route team
    Route::get('/team', ['as' => 'frontend.team.index', 'uses' => 'Frontend\TeamController@index']);
    Route::delete('/team/delete/{id}', ['as' => 'frontend.team.destroy', 'uses' => 'Frontend\TeamController@destroy']);
    //End route team
    
    //Start route level
    Route::get('/level', ['as' => 'frontend.level.index', 'uses' => 'Frontend\LevelController@index']);
    Route::delete('/level/delete/{id}', ['as' => 'frontend.level.destroy', 'uses' => 'Frontend\LevelController@destroy']);
    //End route level
    
    //Start route position
    Route::get('/position', ['as' => 'frontend.position.index', 'uses' => 'Frontend\PositionController@index']);
    Route::delete('/position/delete/{id}', ['as' => 'frontend.position.destroy', 'uses' => 'Frontend\PositionController@destroy']);
    //End route position
    
    //Start route slide
    Route::get('/slide', ['as' => 'frontend.slide.index', 'uses' => 'Frontend\SlideController@index']);
    Route::delete('/slide/delete/{id}', ['as' => 'frontend.slide.destroy', 'uses' => 'Frontend\SlideController@destroy']);
    //End route slide
    
    //Start route configs
    Route::get('/configs', ['as' => 'frontend.config.index', 'uses' => 'Frontend\ConfigController@index']);
    Route::post('/configs/update', ['as' => 'frontend.config.update', 'uses' => 'Frontend\ConfigController@update']);
    //End route configs
    
     //Start route document
    Route::get('/document', ['as' => 'frontend.document.index', 'uses' => 'Frontend\DocumentController@index']);
    Route::post('/document/update', ['as' => 'frontend.document.update', 'uses' => 'Frontend\DocumentController@update']);
    Route::delete('/document/delete/{id}', ['as' => 'frontend.document.destroy', 'uses' => 'Frontend\DocumentController@destroy']);
    //End route document
});
