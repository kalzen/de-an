<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
/*
  Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
  });
 */
Route::post('/get-product-attribute', ['as' => 'api.get_product_attribute', 'uses' => 'Api\ProductController@getProductAttribute']);
Route::post('/get-sale-product-attribute', ['as' => 'api.get_sale_product_attribute', 'uses' => 'Api\ProductController@getSaleProductAttribute']);
Route::post('/slugify', ['as' => 'api.slugify', 'uses' => 'Api\BackendController@slugify']);
Route::post('/change-status', ['as' => 'api.change_status', 'uses' => 'Api\BackendController@changeStatus']);
Route::post('/save-tag', ['as' => 'api.save_tag', 'uses' => 'Backend\TagController@add']);
Route::post('/update-tag', ['as' => 'api.update_tag', 'uses' => 'Backend\TagController@update']);
Route::post('/remove-tag', ['as' => 'api.remove_tag', 'uses' => 'Backend\TagController@delete']);
Route::post('/get-tags', ['as' => 'api.get_tags', 'uses' => 'Backend\TagController@getTagsByGalleryId']);
Route::post('/add-subscriber', ['as' => 'api.add_subscriber', 'uses' => 'Api\FrontendController@addSubscriber']);
Route::post('/get-recent-post', ['as' => 'api.get_recent_post', 'uses' => 'Api\FrontendController@getRecentPost']);
Route::post('/get-recent-post-mobile', ['as' => 'api.get_recent_post_mobile', 'uses' => 'Api\FrontendController@getRecentPostMobile']);
Route::post('/register-marketing', ['as' => 'api.register-marketing', 'uses' => 'Api\FrontendController@registerMarketing']);
Route::post('/register-member', ['as' => 'api.register-member', 'uses' => 'Api\FrontendController@registerMember']);
Route::post('/register-construction', ['as' => 'api.register-construction', 'uses' => 'Api\FrontendController@registerConstruction']);
Route::post('/check-user-marketing', ['as' => 'api.check-user-marketing', 'uses' => 'Api\FrontendController@checkUserMarketing']);
Route::post('/check-user-construction', ['as' => 'api.check-user-construction', 'uses' => 'Api\FrontendController@checkUserConstruction']);
Route::post('/get-products', ['as' => 'api.get_products', 'uses' => 'Api\FrontendController@getProducts']);
Route::post('/get-gallery-tags', ['as' => 'api.get_gallery_tags', 'uses' => 'Api\FrontendController@getTags']);
Route::post('/get-products-by-tag', ['as' => 'api.get_products_by_tag', 'uses' => 'Api\FrontendController@getProductsByTag']);
Route::post('/send-contact', ['as' => 'api.send_contact', 'uses' => 'Api\FrontendController@sendContact']);
Route::post('/add-review', ['as' => 'api.add_review', 'uses' => 'Api\ReviewController@addReview']);
Route::post('/register-member', ['as' => 'api.register-member', 'uses' => 'Api\FrontendController@registerMember']);
Route::post('/upload', ['as' => 'api.upload', 'uses' => 'Api\FrontendController@upload']);
Route::post('/uploadImage', ['as' => 'api.uploadimage', 'uses' => 'Api\FrontendController@uploadImage']);
Route::post('/delete_image', ['as' => 'api.upload', 'uses' => 'Api\FrontendController@delete_image']);
Route::post('/edit-list-member', ['as' => 'api.edit-list-member', 'uses' => 'Api\MemberController@editList']);
Route::post('/edit-detail-member', ['as' => 'api.edit-detail-member', 'uses' => 'Api\MemberController@editDetail']);
Route::post('/update-member', ['as' => 'api.update-member', 'uses' => 'Api\MemberController@update']);
Route::post('/next-page-member', ['as' => 'api.next-page-member', 'uses' => 'Api\MemberController@nextPage']);
Route::post('/forward-page-member', ['as' => 'api.forword-page-member', 'uses' => 'Api\MemberController@forwardPage']);
Route::post('/next-list-project', ['as' => 'api.next-list-project', 'uses' => 'Api\ProjectController@nextList']);
Route::post('/forward-list-project', ['as' => 'api.forword-list-project', 'uses' => 'Api\ProjectController@forwardList']);
Route::post('/remove-member', ['as' => 'api.remove-member', 'uses' => 'Api\MemberController@destroy']);
Route::post('/list-member', ['as' => 'api.list-member', 'uses' => 'Api\MemberController@listMember']);
Route::post('/list-member-remove', ['as' => 'api.list-member-remove', 'uses' => 'Api\MemberController@listMemberRemove']);
Route::post('/restore-member', ['as' => 'api.restore-member', 'uses' => 'Api\MemberController@restoreMember']);
Route::post('/search-member', ['as' => 'api.search-member', 'uses' => 'Api\MemberController@searchMember']);
Route::post('/search-project', ['as' => 'api.search-project', 'uses' => 'Api\ProjectController@searchProject']);
Route::post('/next-page-project', ['as' => 'api.next-page-project', 'uses' => 'Api\ProjectController@nextPage']);
Route::post('/forward-page-project', ['as' => 'api.forword-page-project', 'uses' => 'Api\ProjectController@forwardPage']);
Route::post('/save-project', ['as' => 'api.save-project', 'uses' => 'Api\ProjectController@save']);
Route::post('/send-project', ['as' => 'api.send-project', 'uses' => 'Api\ProjectController@send']);
Route::post('/submit-project', ['as' => 'api.submit-project', 'uses' => 'Api\ProjectController@submit']);
Route::post('/remove-project', ['as' => 'api.remove-project', 'uses' => 'Api\ProjectController@destroy']);
Route::post('/check-password', ['as' => 'api.check-password', 'uses' => 'Api\MemberController@checkPassword']);
Route::post('/reset-password', ['as' => 'api.reset-password', 'uses' => 'Api\MemberController@resetPassword']);
Route::post('/getInfoMember', ['as' => 'api.info-member', 'uses' => 'Api\MemberController@getInfo']);
Route::post('/getListProject', ['as' => 'api.get-project', 'uses' => 'Api\ProjectController@getListProject']);
Route::post('/return-project', ['as' => 'api.return-project', 'uses' => 'Api\ProjectController@returnProject']);
Route::post('/add-part', ['as' => 'api.part.store', 'uses' => 'Api\FrontendController@addPart']);
Route::post('/edit-part', ['as' => 'api.part.edit', 'uses' => 'Api\FrontendController@editPart']);
Route::post('/update-part', ['as' => 'api.part.update', 'uses' => 'Api\FrontendController@updatePart']);
Route::post('/add-department', ['as' => 'api.department.store', 'uses' => 'Api\FrontendController@addDepartment']);
Route::post('/edit-department', ['as' => 'api.department.edit', 'uses' => 'Api\FrontendController@editDepartment']);
Route::post('/update-department', ['as' => 'api.department.update', 'uses' => 'Api\FrontendController@updateDepartment']);
Route::post('/add-groups', ['as' => 'api.groups.store', 'uses' => 'Api\FrontendController@addGroups']);
Route::post('/edit-groups', ['as' => 'api.groups.edit', 'uses' => 'Api\FrontendController@editGroups']);
Route::post('/update-groups', ['as' => 'api.groups.update', 'uses' => 'Api\FrontendController@updateGroups']);
Route::post('/add-team', ['as' => 'api.team.store', 'uses' => 'Api\FrontendController@addTeam']);
Route::post('/edit-team', ['as' => 'api.team.edit', 'uses' => 'Api\FrontendController@editTeam']);
Route::post('/update-team', ['as' => 'api.team.update', 'uses' => 'Api\FrontendController@updateTeam']);
Route::post('/add-level', ['as' => 'api.level.store', 'uses' => 'Api\FrontendController@addLevel']);
Route::post('/edit-level', ['as' => 'api.level.edit', 'uses' => 'Api\FrontendController@editLevel']);
Route::post('/update-level', ['as' => 'api.level.update', 'uses' => 'Api\FrontendController@updateLevel']);
Route::post('/add-position', ['as' => 'api.position.store', 'uses' => 'Api\FrontendController@addPosition']);
Route::post('/edit-position', ['as' => 'api.position.edit', 'uses' => 'Api\FrontendController@editPosition']);
Route::post('/update-position', ['as' => 'api.position.update', 'uses' => 'Api\FrontendController@updatePosition']);
Route::post('/add-slide', ['as' => 'api.slide.store', 'uses' => 'Api\FrontendController@addSlide']);
Route::post('/edit-slide', ['as' => 'api.slide.edit', 'uses' => 'Api\FrontendController@editSlide']);
Route::post('/update-slide', ['as' => 'api.slide.update', 'uses' => 'Api\FrontendController@updateSlide']);
Route::post('/add-document', ['as' => 'api.document.store', 'uses' => 'Api\FrontendController@addDocument']);
Route::post('/edit-document', ['as' => 'api.document.edit', 'uses' => 'Api\FrontendController@editDocument']);
Route::post('/update-document', ['as' => 'api.document.update', 'uses' => 'Api\FrontendController@updateDocument']);
Route::post('/import-member', ['as' => 'api.member.import', 'uses' => 'Api\MemberController@import']);
Route::post('/export-member', ['as' => 'api.member.export', 'uses' => 'Api\MemberController@export']);
Route::post('/export-project', ['as' => 'api.project.export', 'uses' => 'Api\ProjectController@export']);
Route::post('/export-member-project', ['as' => 'api.project.export', 'uses' => 'Api\ProjectController@exportProject']);
Route::post('/get-all-message', ['as' => 'api.get-all-message', 'uses' => 'Api\MemberController@getAllMessage']);
Route::post('/get-message','Api\MemberController@getMessage');
Route::post('/send-message','Api\MemberController@sendMessage');
Route::post('/send-file-message','Api\MemberController@sendFileMessage');
Route::post('/seen-notification', ['as' => 'frontend.project.seen', 'uses' => 'Api\MemberController@seenNotification']);
Route::post('/getInfoMemberMoblie', ['as' => 'api.info-member-mobile', 'uses' => 'Api\MemberController@getInfoMobile']);
Route::post('/update-avatar', ['as' => 'api.avatar.update', 'uses' => 'Api\MemberController@updateAvatar']);
Route::post('/checkid-member', ['as' => 'api.member.checkid', 'uses' => 'Api\MemberController@checkID']);
Route::post('/add-equipment', ['as' => 'api.equipment.store', 'uses' => 'Api\FrontendController@addEquipment']);
Route::post('/edit-equipment', ['as' => 'api.equipment.edit', 'uses' => 'Api\FrontendController@editEquipment']);
Route::post('/update-equipment', ['as' => 'api.equipment.update', 'uses' => 'Api\FrontendController@updateEquipment']);
Route::post('/add-work', ['as' => 'api.work.store', 'uses' => 'Api\FrontendController@addWork']);
Route::post('/edit-work', ['as' => 'api.work.edit', 'uses' => 'Api\FrontendController@editWork']);
Route::post('/update-work', ['as' => 'api.work.update', 'uses' => 'Api\FrontendController@updateWork']);
Route::post('/next-schedule', ['as' => 'api.schedule.next', 'uses' => 'Api\ScheduleController@next']);
Route::post('/prev-schedule', ['as' => 'api.schedule.prev', 'uses' => 'Api\ScheduleController@prev']);
Route::post('/change-schedule', ['as' => 'api.schedule.change', 'uses' => 'Api\ScheduleController@change']);
Route::post('/get-list-info', ['as' => 'api.schedule.getlist', 'uses' => 'Api\ScheduleController@getListInfo']);
Route::post('/create-todolist', ['as' => 'api.todolist.add', 'uses' => 'Api\TodolistController@add']);
Route::post('/change-status-todolist', ['as' => 'api.todolist.change', 'uses' => 'Api\TodolistController@change']);
Route::post('/check-schedule', ['as' => 'api.schedule.check', 'uses' => 'Api\ScheduleController@check']);
Route::post('/check-duplicate_schedule', ['as' => 'api.schedule.checkduplicate', 'uses' => 'Api\ScheduleController@checkDuplicate']);
Route::post('/update-level-project',['uses'=>'Api\ProjectController@updateLevel']);
Route::post('/update-device-token',['uses'=>'Api\MemberController@updateToken']);

